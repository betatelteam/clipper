/**
 * Created by Muris Agic on 08/10/2016.
 */

'use strict';

var models = require('../db/models');

module.exports = function (app) {

    /**USERS - ALL*/
    app.get('/api/users', function (req, res) {
        models.User.findAll({
            include: [{model: models.Role}, {model: models.UserStatus, attributes: ['name']}]
        }).then(function (users) {
            users.filter(function (user) {
                    return user.Roles.length > 0
                })
                .forEach(function (user) {
                    user.dataValues.role_names = user.Roles.map(function (role) {
                        return role.name
                    }).join(',')
                });
            return res.send(users);
        });
    });

    /**USER ISSUES - ALL*/
    app.get('/api/users/issues', function (req, res) {
        models.UserToIssue.findAll({include: [{model: models.User}]
        }).then(function (result) {
            return res.send(result);
        });
    });

    /**USER ISSUES - BY USER*/
    app.get('/api/users/:userId/issues', function (req, res) {
        models.UserToIssue.findAll({where: {user_id: req.params.userId},
            include: [{model: models.User}]
        }).then(function (result) {
            return res.send(result);
        });
    });

    /**ROLES - ALL*/
    app.get('/api/roles', function (req, res) {
        models.Role.findAll().then(function (roles) {
            return res.send(roles);
        });
    });

    /**ROLES - BY ID*/
    app.get('/api/roles/:roleId', function (req, res) {
        models.Role.find({where: {id: req.params.roleId}}).then(function (roles) {
            return res.send(roles);
        });
    });

    /**ROLE PRIVILEGES - ALL*/
    app.get('/api/role_privileges', function (req, res) {
        models.RolePrivilege.findAll().then(function (rolePrivileges) {
            return res.send(rolePrivileges);
        });
    });

    /**USER STATUSES - ALL*/
    app.get('/api/user_statuses', function (req, res) {
        models.UserStatus.findAll().then(function (statuses) {
            return res.send(statuses);
        });
    });

    /**LANGUAGES - ALL*/
    app.get('/api/languages', function (req, res) {
        models.Language.findAll().then(function (languages) {
            return res.send(languages);
        });
    });

    /**LANGUAGES - BY ID*/
    app.get('/api/languages/:languageId', function (req, res) {
        models.Language.find({where: {id: req.params.languageId}}).then(function (language) {
            return res.send(language);
        });
    });

    /**USERS - CREATE & UPDATE*/
    app.post('/api/users', function (req, res) {
        if (req.body) {
            var user = models.User.build(req.body, {isNewRecord: req.body.id == null});
            user.save().then(function (user) {

                if (req.body.id != null)
                    models.UserToRole.destroy({
                        where: {user_id: req.body.id}
                    });

                user.setRoles(req.body.Roles.map(function (role) {
                    return role.id
                }), {save: true}).then(function () {
                    user.reload({include: [{model: models.Role}, {model: models.UserStatus}]}).then(function (user) {
                        user.dataValues.role_names = user.Roles.map(function (role) {
                            return role.name
                        }).join(',');
                        return res.send({code: 200, data: user});
                    });
                }).catch(function (error) {
                    return res.send(error);
                });
            });
        }
    });

    /**USERS - CHANGE LANGUAGE*/
    app.post('/api/change_language', function (req, res) {
        if (req.body) {
            models.Language.find({where: {language_key: req.body.language_key}}).then(function (language) {
                if (!language)
                    return res.send({code: 100});
                models.User.update({default_language_id: language.id}, {where: {id: req.body.user_id}}).then(function () {
                    return res.send({code: 200});
                }).catch(function (error) {
                    return res.send(error);
                });
            }).catch(function (error) {
                return res.send(error);
            });
        }
    });
}
;

