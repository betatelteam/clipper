/**
 * Created by Muris Agic on 08/10/2016.
 */

'use strict';

var models = require('../db/models');

module.exports = function (app) {

    /**CALLS - ALL*/
    app.get('/api/report/calls', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id',[models.Sequelize.fn('count', models.Sequelize.col('call_id')),'count_number']],
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER*/
    app.get('/api/report/calls/user/:user_id', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id',[models.Sequelize.fn('count', models.Sequelize.col('call_id')),'count_number']],
            where: {user_id: req.params.user_id},
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY OPERATOR*/
    app.get('/api/report/calls/operator/:operator_id', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id',[models.Sequelize.fn('count', models.Sequelize.col('call_id')),'count_number']],
            where: {in_operator_id: req.params.operator_id},
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER AND OPERATOR*/
    app.get('/api/report/calls/user/:user_id/operator/:operator_id', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id',[models.Sequelize.fn('count', models.Sequelize.col('call_id')),'count_number']],
            where: {user_id: req.params.user_id, in_operator_id: req.params.operator_id},
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY YEAR*/
    app.get('/api/report/calls/year/:year', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id',[models.Sequelize.fn('count', models.Sequelize.col('call_id')),'count_number']],
            where : models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER AND YEAR*/
    app.get('/api/report/calls/user/:user_id/year/:year', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({user_id: req.params.user_id}, models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY OPERATOR AND YEAR*/
    app.get('/api/report/calls/operator/:operator_id/year/:year', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({in_operator_id: req.params.operator_id}, models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER, OPERATOR AND YEAR*/
    app.get('/api/report/calls/user/:user_id/operator/:operator_id/year/:year', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id}, models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY MONTH*/
    app.get('/api/report/calls/year/:year/month/:month', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER AND MONTH*/
    app.get('/api/report/calls/user/:user_id/year/:year/month/:month', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({user_id: req.params.user_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year))),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY OPERATOR AND MONTH*/
    app.get('/api/report/calls/operator/:operator_id/year/:year/month/:month', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year))),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER, OPERATOR AND MONTH*/
    app.get('/api/report/calls/user/:user_id/operator/:operator_id/year/:year/month/:month', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year))),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY WEEK*/
    app.get('/api/report/calls/year/:year/week/:week', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER AND WEEK*/
    app.get('/api/report/calls/user/:user_id/year/:year/week/:week', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where:  models.Sequelize.and({user_id: req.params.user_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year))),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY OPERATOR AND WEEK*/
    app.get('/api/report/calls/operator/:operator_id/year/:year/week/:week', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where:  models.Sequelize.and({in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year))),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER, OPERATOR AND WEEK*/
    app.get('/api/report/calls/user/:user_id/operator/:operator_id/year/:year/week/:week', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where:  models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year))),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY DATE*/
    app.get('/api/report/calls/date/:date', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER AND DATE*/
    app.get('/api/report/calls/user/:user_id/date/:date', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({user_id: req.params.user_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY OPERATOR AND DATE*/
    app.get('/api/report/calls/operator/:operator_id/date/:date', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**CALLS - BY USER, OPERATOR AND DATE*/
    app.get('/api/report/calls/user/:user_id/operator/:operator_id/date/:date', function (req, res) {
        models.CallStat.findAll({
            attributes: ['call_status_id', [models.Sequelize.fn('count', models.Sequelize.col('call_id')), 'count_number']],
            where: models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)),
            group: ['call_status_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - ALL*/
    app.get('/api/report/operators', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER*/
    app.get('/api/report/operators/user/:user_id', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: {user_id: req.params.user_id},
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY OPERATOR*/
    app.get('/api/report/operators/operator/:operator_id', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: {in_operator_id: req.params.operator_id},
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER AND OPERATOR*/
    app.get('/api/report/operators/user/:user_id/operator/:operator_id', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: {user_id: req.params.user_id, in_operator_id: req.params.operator_id},
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY YEAR*/
    app.get('/api/report/operators/year/:year', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where : models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER AND YEAR*/
    app.get('/api/report/operators/user/:user_id/year/:year', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({user_id: req.params.user_id}, models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY OPERATOR AND YEAR*/
    app.get('/api/report/operators/operator/:operator_id/year/:year', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({in_operator_id: req.params.operator_id}, models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER, OPERATOR AND YEAR*/
    app.get('/api/report/operators/user/:user_id/operator/:operator_id/year/:year', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id}, models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY MONTH*/
    app.get('/api/report/operators/year/:year/month/:month', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER AND MONTH*/
    app.get('/api/report/operators/user/:user_id/year/:year/month/:month', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({user_id: req.params.user_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year))),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY OPERATOR AND MONTH*/
    app.get('/api/report/operators/operator/:operator_id/year/:year/month/:month', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year))),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER, OPERATOR AND MONTH*/
    app.get('/api/report/operators/user/:user_id/operator/:operator_id/year/:year/month/:month', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-', req.params.year))),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY WEEK*/
    app.get('/api/report/operators/year/:year/week/:week', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER AND WEEK*/
    app.get('/api/report/operators/user/:user_id/year/:year/week/:week', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where:  models.Sequelize.and({user_id: req.params.user_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year))),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY OPERATOR AND WEEK*/
    app.get('/api/report/operators/operator/:operator_id/year/:year/week/:week', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where:  models.Sequelize.and({in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year))),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER, OPERATOR AND WEEK*/
    app.get('/api/report/operators/user/:user_id/operator/:operator_id/year/:year/week/:week', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where:  models.Sequelize.and({user_id: req.params.user_id,in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-', req.params.year))),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY DATE*/
    app.get('/api/report/operators/date/:date', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER AND DATE*/
    app.get('/api/report/operators/user/:user_id/date/:date', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({user_id: req.params.user_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY OPERATOR AND DATE*/
    app.get('/api/report/operators/operator/:operator_id/date/:date', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - BY USER, OPERATOR AND DATE*/
    app.get('/api/report/operators/user/:user_id/operator/:operator_id/date/:date', function (req, res) {
        models.OperatorStat.findAll({
            attributes: ['operator_name','country_name',[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',1)),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',2)),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.where(models.Sequelize.col('call_status_id'),'=',3)),'count_unsuccessful']],
            where: models.Sequelize.and({user_id: req.params.user_id, in_operator_id: req.params.operator_id},models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)),
            group: ['operator_id']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - MONTHLY GRAPH - ALL*/
    app.get('/api/report/operators_monthly', function (req, res) {
        models.OperatorMonthlyStat.findAll({
            attributes: ['month',[models.Sequelize.fn('sum', models.Sequelize.col('count_correct')),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.col('count_fraud')),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.col('count_unsuccessful')),'count_unsuccessful']],
            group: ['month']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - MONTHLY GRAPH - BY USER*/
    app.get('/api/report/operators_monthly/user/:user_id', function (req, res) {
        models.OperatorMonthlyStat.findAll({
            attributes: ['month',[models.Sequelize.fn('sum', models.Sequelize.col('count_correct')),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.col('count_fraud')),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.col('count_unsuccessful')),'count_unsuccessful']],
            where: {user_id: req.params.user_id},
            group: ['month']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - MONTHLY GRAPH - BY OPERATOR*/
    app.get('/api/report/operators_monthly/operator/:operator_id', function (req, res) {
        models.OperatorMonthlyStat.findAll({
            attributes: ['month',[models.Sequelize.fn('sum', models.Sequelize.col('count_correct')),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.col('count_fraud')),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.col('count_unsuccessful')),'count_unsuccessful']],
            where: {in_operator_id: req.params.operator_id},
            group: ['month']
        }).then(function (stat) {
            return res.send(stat);
        });
    });

    /**OPERATORS - MONTHLY GRAPH - BY USER AND OPERATOR*/
    app.get('/api/report/operators_monthly/user/:user_id/operator/:operator_id', function (req, res) {
        models.OperatorMonthlyStat.findAll({
            attributes: ['month',[models.Sequelize.fn('sum', models.Sequelize.col('count_correct')),'count_correct'],[models.Sequelize.fn('sum', models.Sequelize.col('count_fraud')),'count_fraud'],[models.Sequelize.fn('sum', models.Sequelize.col('count_unsuccessful')),'count_unsuccessful']],
            where: {user_id: req.params.user_id, in_operator_id: req.params.operator_id},
            group: ['month']
        }).then(function (stat) {
            return res.send(stat);
        });
    });
};