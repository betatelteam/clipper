/**
 * Created by Muris Agic on 08/16/2016.
 */

'use strict';

var models = require('../db/models');

module.exports = function (app) {

    /**COUNTRY - ALL*/
    app.get('/api/countries', function (req, res) {
        models.Country.findAll({include:[{model:models.Operator,include:[models.Number]}]}).then(function (countries) {
            return res.send(countries);
        });
    });

    /**COUNTRY - INBOUND*/
    app.get('/api/countries/inbound', function (req, res) {
        models.Country.findAll({include:[{model:models.Operator,include:[{model:models.UserToOperator,where:{type:'in'}}]}]}).then(function (countries) {
            return res.send(countries);
        });
    });

    /**COUNTRY - INBOUND - BY USER*/
    app.get('/api/countries/inbound/user/:user_id', function (req, res) {
        models.Country.findAll({include:[{model:models.Operator,include:[{model:models.UserToOperator,where:{user_id:req.params.user_id,type:'in'}}]}]}).then(function (countries) {
            return res.send(countries);
        });
    });

    /**COUNTRY - OUTBOUND*/
    app.get('/api/countries/outbound/', function (req, res) {
        models.Country.findAll({include:[{model:models.Operator,include:[{model:models.UserToOperator,where:{type:'out'}}]}]}).then(function (countries) {
            return res.send(countries);
        });
    });

    /**COUNTRY - OUTBOUND - BY USER*/
    app.get('/api/countries/outbound/user/:user_id', function (req, res) {
        models.Country.findAll({include:[{model:models.Operator,include:[{model:models.UserToOperator,where:{user_id:req.params.user_id,type:'out'}}]}]}).then(function (countries) {
            return res.send(countries);
        });
    });

    /**COUNTRY - CREATE & UPDATE*/
    app.post('/api/countries', function (req, res) {
        if (req.body) {
            var type = models.Country.build(req.body, {isNewRecord: req.body.id == null});
            type.save().then(function (result) {
                return res.send({code: 200, data: result});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**COUNTRY - DELETE*/
    app.delete('/api/countries', function (req, res) {
        if (req.query.id) {
            models.Country.destroy({
                where: {id: req.query.id}
            }).then(function () {
                res.send({code: 200});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    })
};