/**
 * Created by Muris Agic on 8/10/2016.
 */

'use strict';

var models = require('../db/models');
var globals = require('../global');

module.exports = function (app) {

    /**OUTGOING CALLS - ALL*/
    app.get('/api/outgoing_calls', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,
            include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            order:[['start_stamp','DESC']]}).then(function (calls) {
                return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER*/
    app.get('/api/outgoing_calls/user/:user_id', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,
            where: {user_id: req.params.user_id},
            include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            order:[['start_stamp','DESC']]}).then(function (calls) {
                return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER AND OPERATOR*/
    app.get('/api/outgoing_calls/operator/:operator_id', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call, include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},
            {model:models.Number, as: 'DestinationNumber', include:[{model:models.Operator}], where:{operator_id: req.params.operator_id}}]}],
            order:[['start_stamp','DESC']]}).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER AND OPERATOR*/
    app.get('/api/outgoing_calls/user/:user_id/operator/:operator_id', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,
            where: {user_id: req.params.user_id},
            include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber', include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}]
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY YEAR*/
    app.get('/api/outgoing_calls/year/:year', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call, include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER AND YEAR*/
    app.get('/api/outgoing_calls/user/:user_id/year/:year', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY OPERATOR AND YEAR*/
    app.get('/api/outgoing_calls/operator/:operator_id/year/:year', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:[{model:models.Operator}]},{model:models.Number, as: 'DestinationNumber',
            include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER, OPERATOR AND YEAR*/
    app.get('/api/outgoing_calls/user/:user_id/operator/:operator_id/year/:year', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber'},{model:models.Number, as: 'DestinationNumber',
            include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('start_stamp')), req.params.year)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY MONTH*/
    app.get('/api/outgoing_calls/year/:year/month/:month', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER AND MONTH*/
    app.get('/api/outgoing_calls/user/:user_id/year/:year/month/:month', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY OPERATOR AND MONTH*/
    app.get('/api/outgoing_calls/operator/:operator_id/year/:year/month/:month', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:[{model:models.Operator}]},{model:models.Number, as: 'DestinationNumber',
            include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER, OPERATOR AND MONTH*/
    app.get('/api/outgoing_calls/user/:user_id/operator/:operator_id/year/:year/month/:month', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',
            include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%m-%Y'), '=', req.params.month.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY WEEK*/
    app.get('/api/outgoing_calls/year/:year/week/:week', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER AND WEEK*/
    app.get('/api/outgoing_calls/user/:user_id/year/:year/week/:week', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY OPERATOR AND WEEK*/
    app.get('/api/outgoing_calls/operator/:operator_id/year/:year/week/:week', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',
            include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER, OPERATOR AND WEEK*/
    app.get('/api/outgoing_calls/user/:user_id/operator/:operator_id/year/:year/week/:week', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',
            include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%u-%Y'), '=', req.params.week.concat('-',req.params.year))
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY DATE*/
    app.get('/api/outgoing_calls/date/:date/', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER AND DATE*/
    app.get('/api/outgoing_calls/user/:user_id/date/:date/', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},{model:models.Number, as: 'DestinationNumber',include:{model:models.Operator}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY OPERATOR AND DATE*/
    app.get('/api/outgoing_calls/operator/:operator_id/date/:date/', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},
            {model:models.Number, as: 'DestinationNumber', include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**OUTGOING CALLS - BY USER, OPERATOR AND DATE*/
    app.get('/api/outgoing_calls/user/:user_id/operator/:operator_id/date/:date/', function (req, res) {
        models.CdrOutgoingCallView.findAll({include:[{model:models.Call, where: {user_id: req.params.user_id},include:[{model:models.Number, as: 'SourceNumber',include:{model:models.Operator}},
            {model:models.Number, as: 'DestinationNumber', include:[{model:models.Operator}],where:{operator_id: req.params.operator_id}}]}],
            where : models.Sequelize.where(models.Sequelize.fn('date_format', models.Sequelize.col('start_stamp'), '%d-%m-%Y'), '=', req.params.date)
        }).then(function (calls) {
            return res.send(calls);
        });
    });

    /**INCOMING CALLS - BY CALL*/
    app.get('/api/outgoing_calls/:call_id/incoming_calls', function (req, res) {
        models.CdrIncomingCall.findAll({include:[{model:models.Call,include:[{model:models.Number, as: 'DestinationNumber',include:[{model:models.Operator}]}]}],
            where: {call_id: req.params.call_id}}).then(function (calls) {
            return res.send(calls);
        });
    });

    /**CALL STATUSES*/
    app.get('/api/call_statuses', function (req, res) {
        models.CallStatus.findAll().then(function (statuses) {
            return res.send(statuses);
        });
    });

    /**MAKE NEW CALL*/
    app.post('/api/new_call', function (req, res) {
        return res.send({code:globals.scheduler.startRandomCall(0,req.body.id)})
    });
};
