/**
 * Created by Muris Agic on 08/10/2016.
 */

'use strict';

var models = require('../db/models');
var globals = require('../global');

module.exports = function (app) {

    /**GET ALL TASKS*/
    app.get('/api/scheduled_tasks', function (req, res) {
        models.Scheduler.findAll({
            include: [models.User]
        }).then(function (tasks) {
            return res.send(tasks);
        });
    });

    /**GET TASK BY ID*/
    app.get('/api/scheduled_tasks/user/:user_id', function (req, res) {
        models.Scheduler.findAll({
            include: [models.User], where: {user_id: req.params.user_id}
        }).then(function (tasks) {
            return res.send(tasks);
        });
    });

    /**CREATE & UPDATE*/
    app.post('/api/scheduled_tasks', function (req, res) {
        if (req.body) {
            var scheduler = models.Scheduler.build(req.body, {isNewRecord: req.body.id == null});
            scheduler.save().then(function () {
                scheduler.reload({include: [models.User]}).then(function () {
                    if (req.body.id != null)
                        var code = globals.scheduler.changeStatus(req.body.id, req.body.active);
                    else
                        code = globals.scheduler.refreshScheduledCalls();
                    return res.send({code: code, data: scheduler});
                });
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**DELETE*/
    app.delete('/api/scheduled_tasks', function (req, res) {
        if (req.query.id) {
            models.Scheduler.destroy({
                where: {id: req.query.id}
            }).then(function () {
                return res.send({code: globals.scheduler.changeStatus(req.body.id, req.body.active)});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**REFRESH SCHEDULER CALLS*/
    app.post('/api/refresh_scheduler', function (req, res) {
        return res.send({code: globals.scheduler.refreshScheduledCalls()})
    });
};

