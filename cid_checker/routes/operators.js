/**
 * Created by Muris Agic on 08/16/2016.
 */

'use strict';

var models = require('../db/models');

module.exports = function (app) {

    /**OPERATOR - ALL*/
    app.get('/api/operators', function (req, res) {
        models.Operator.findAll({include: [{model: models.Country},{model: models.UserToOperator}, {model:models.Number,include:[{model:models.Gateway}]}]}).then(function (operators) {
            return res.send(operators);
        });
    });

    /**OPERATOR CODES - ALL*/
    app.get('/api/operator_codes', function (req, res) {
        models.OperatorCode.findAll().then(function (codes) {
            return res.send(codes);
        });
    });

    /**OPERATOR CODES - BY OPERATOR*/
    app.get('/api/operators/:operatorId/operator_codes', function (req, res) {
        models.OperatorCode.findAll({where: {operator_id: req.params.operatorId}}).then(function (codes) {
            return res.send(codes);
        });
    });

    /**OPERATOR - INBOUND*/
    app.get('/api/operators/inbound', function (req, res) {
        models.Operator.findAll({include: [{model: models.Country},{model: models.UserToOperator,where:{type:'in'}}],
        group:['id']}).then(function (operators) {
            return res.send(operators);
        });
    });

    /**OPERATOR - INBOUND BY USER*/
    app.get('/api/operators/inbound/user/:user_id', function (req, res) {
        models.Operator.findAll({include: [{model: models.Country},{model: models.UserToOperator,where:{type:'in', user_id:req.params.user_id}}],
            group:['id']}).then(function (operators) {
            return res.send(operators);
        });
    });

    /**OPERATOR - OUTBOUND*/
    app.get('/api/operators/outbound', function (req, res) {
        models.Operator.findAll({include: [{model: models.Country},{model: models.UserToOperator,where:{type:'out'}}],
            group:['id']}).then(function (operators) {
            return res.send(operators);
        });
    });

    /**OPERATOR - OUTBOUND BY USER*/
    app.get('/api/operators/outbound/user/:user_id', function (req, res) {
        models.Operator.findAll({include: [{model: models.Country},{model: models.UserToOperator,where:{type:'out', user_id:req.params.user_id}}],
            group:['id']}).then(function (operators) {
            return res.send(operators);
        });
    });

    /**OPERATOR - CREATE & UPDATE*/
    app.post('/api/operators', function (req, res) {
        if (req.body) {
            var operator = models.Operator.build(req.body, {isNewRecord: req.body.id == null});
            operator.save().then(function () {
                operator.reload({include: [models.Country]}).then(function () {
                    return res.send({code: 200, data: operator});
                });
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**OPERATOR CODES - CREATE & UPDATE*/
    app.post('/api/operators/operator_codes', function (req, res) {
        if (req.body) {
            var operatorCode = models.OperatorCode.build(req.body, {isNewRecord: req.body.id == null});
            operatorCode.save().then(function (result) {
                return res.send({code: 200, data: result});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**OPERATOR - DELETE*/
    app.delete('/api/operators', function (req, res) {
        if (req.query.id) {
            models.Operator.destroy({
                where: {id: req.query.id}
            }).then(function () {
                res.send({code: 200});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**OPERATOR CODE - DELETE*/
    app.delete('/api/operators/operator_codes', function (req, res) {
        if (req.query.id) {
            models.OperatorCode.destroy({
                where: {id: req.query.id}
            }).then(function () {
                res.send({code: 200});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    })
};
