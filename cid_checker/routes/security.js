/**
 * Created by Muris Agic 08/10/2016.
 */

'use strict';

var express = require('express');
var jwt = require('jsonwebtoken');
var expressJWT = require('express-jwt');
var models = require('../db/models');
var fs = require('fs');
var ini = require('ini');
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

module.exports = function (app) {

    /* Check user */
    app.use('/*', expressJWT({secret: config.security.secret}).unless({path: ['/api/login']}));

    /* Ping */
    app.get('/api/ping', function (req, res) {
        res.status(200).send();
    });

    /* Login */
    app.post('/api/login', function (req, res) {
        models.User.checkLogin(req.body.username, req.body.password, models).then(function (user) {
            if (user) {
                if (user.user_status_id == 1) {
                    if (user.Roles)
                        user.role_names = user.Roles.map(function(elem){
                            return elem.name;
                        }).join(", ");
                    var token = jwt.sign(user.toJSON(), config.security.secret, {expiresIn: 16000});
                    res.json({
                        user: {
                            id: user.id,
                            username: user.username,
                            user_status_id: user.user_status_id,
                            first_name: user.first_name,
                            last_name: user.last_name,
                            Roles: user.Roles,
                            Language: user.Language,
                            role_names: user.role_names
                        }, token: token
                    });
                }
                else
                    res.status(409).send();
            }
            else
                res.status(401).send();
        });
    });
};
