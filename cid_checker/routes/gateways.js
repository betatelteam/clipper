/**
 * Created by Muris Agic on 08/16/2016.
 */

'use strict';

var models = require('../db/models');

module.exports = function (app) {

    /**GATEWAY - ALL*/
    app.get('/api/gateways', function (req, res) {
        models.Gateway.findAll({include: [models.Country]}).then(function (gateways) {
            return res.send(gateways);
        });
    });

    /**GATEWAY - CREATE & UPDATE*/
    app.post('/api/gateways', function (req, res) {
        if (req.body) {
            var gateway = models.Gateway.build(req.body, {isNewRecord: req.body.id == null});
            gateway.save().then(function (result) {
                return res.send({code: 200, data: result});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**GATEWAY - DELETE*/
    app.delete('/api/gateways', function (req, res) {
        if (req.query.id) {
            models.Gateway.destroy({
                where: {id: req.query.id}
            }).then(function () {
                res.send({code: 200});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**NUMBER - BY  GATEWAY*/
    app.get('/api/gateways/:gatewayId/numbers', function (req, res) {
        models.Number.findAll({include:[{model: models.Gateway}, {model: models.Operator}],
            where: {gateway_id: req.params.gatewayId},
            order:[['enabled', 'DESC'],['channel_number']]}).then(function (numbers) {
            return res.send(numbers);
        });
    });

    /**NUMBER - CREATE & UPDATE*/
    app.post('/api/numbers', function (req, res) {
        if (req.body) {
            var number = models.Number.build(req.body, {isNewRecord: req.body.id == null});
            number.save().then(function () {
                number.reload({include:[{model: models.Gateway}, {model: models.Operator}]}).then(function () {
                    return res.send({code: 200, data: number});
                });
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });

    /**NUMBER - DELETE*/
    app.delete('/api/numbers', function (req, res) {
        if (req.query.id) {
            models.Number.destroy({
                where: {id: req.query.id}
            }).then(function () {
                res.send({code: 200});
            }).catch(function (error) {
                return res.send(error);
            })
        }
        else
            return res.send("Bad request.");
    });
};
