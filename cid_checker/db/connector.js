/**
 * Created by Muris Agic on 08/09/2016.
 */
'use strict';

var mysql = require('mysql');
var fs = require('fs');
var ini = require('ini');
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

var pool = mysql.createPool({
    host: config.db.host,
    database: config.db.database,
    user: config.db.username,
    password: config.db.password,
    connectionLimit: config.db.connectionLimit
});

module.exports.escapeString = function(val) {
  val = val.replace(/[\0\n\r\b\t\\'"\x1a]/g, function (s) {
    switch (s) {
      case "\0":
        return "\\0";
      case "\n":
        return "\\n";
      case "\r":
        return "\\r";
      case "\b":
        return "\\b";
      case "\t":
        return "\\t";
      case "\x1a":
        return "\\Z";
      case "'":
        return "''";
      case '"':
        return '""';
      default:
        return "\\" + s;
    }
  });

  return val;
};

pool.setMaxListeners(50);

module.exports.getConnection = function (callback, params) {
    return pool.getConnection(function (err, connection) {
        if (err) {
            if (connection)
                connection.release();
            if (config.system.debug)
                console.log(err);
            return null;
        }

        connection.on('error', function (err) {
            if (config.system.debug)
                console.log(err);
            return null;
        });

        callback(connection, params);
    });
};
