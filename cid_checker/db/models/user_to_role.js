/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var UserToRole = sequelize.define('UserToRole', {
            user_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            role_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Role',
                    key: 'id'
                }
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    UserToRole.belongsTo(models.User, {foreignKey: 'user_id'});
                    UserToRole.belongsTo(models.Role, {foreignKey: 'role_id'});
                }
            },
            tableName: 'user2role'
        });
    UserToRole.removeAttribute('id');
    return UserToRole;
};
