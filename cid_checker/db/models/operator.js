/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Operator = sequelize.define('Operator', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        country_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'Country',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
                Operator.belongsTo(models.Country, {foreignKey: 'country_id'});
                Operator.hasMany(models.UserToOperator, {foreignKey: 'operator_id'});
                Operator.hasMany(models.Number, {foreignKey: 'operator_id'});
                Operator.belongsToMany(models.User, { through: models.UserToOperator,foreignKey: 'operator_id'});
                Operator.hasMany(models.OperatorCode, {foreignKey: 'operator_id'});
            }
        },
        tableName: 'operator'
    });
    return Operator;
};
