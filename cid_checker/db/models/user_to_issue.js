/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var UserToIssue = sequelize.define('UserToIssue', {
            issue_id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true
            },
            user_id: {
                type: DataTypes.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    UserToIssue.belongsTo(models.User, {foreignKey: 'user_id'});
                }
            },
            tableName: 'user2issue'
        });
    UserToIssue.removeAttribute('id');
    return UserToIssue;
};
