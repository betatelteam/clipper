/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var RolePrivilege = sequelize.define('RolePrivilege', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            role_id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'Role',
                    key: 'id'
                }
            },
            privilege_id: {
                type: DataTypes.INTEGER(11),
                allowNull: false
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    RolePrivilege.belongsTo(models.Role, {foreignKey: 'role_id'});
                }
            },
            tableName: 'role_privilege'
        });
    return RolePrivilege;
};
