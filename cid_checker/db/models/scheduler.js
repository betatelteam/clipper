/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Scheduler = sequelize.define('Scheduler', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            days: {
                type: DataTypes.INTEGER(3),
                allowNull: true
            },
            scheduler_start: {
                type: DataTypes.DATE,
                allowNull: true
            },
            scheduler_end: {
                type: DataTypes.DATE,
                allowNull: true
            },
            start_time: {
                type: DataTypes.TIME,
                allowNull: false
            },
            end_time: {
                type: DataTypes.TIME,
                allowNull: false
            },
            interval: {
                type: DataTypes.INTEGER(11),
                allowNull: false
            },
            delta_interval: {
                type: DataTypes.INTEGER(11),
                allowNull: true
            },
            active: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
            },
            create_date: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    Scheduler.belongsTo(models.User, {foreignKey: 'user_id'});
                    Scheduler.hasMany(models.ScheduleSpool, {foreignKey:'scheduler_id'})
                }
            },
            tableName: 'scheduler'
        });
    return Scheduler;
};
