/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Number = sequelize.define('Number', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: false
            },
            operator_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Operator',
                    key: 'id'
                }
            },
            gateway_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Gateway',
                    key: 'id'
                }
            },
            channel_number: {
                type: DataTypes.INTEGER(10),
                allowNull: false
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false
            },
            enabled: {
                type: DataTypes.INTEGER(1),
                allowNull: false
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    Number.belongsTo(models.Operator, {foreignKey: 'operator_id'});
                    Number.belongsTo(models.Gateway, {foreignKey: 'gateway_id'});
                    Number.hasMany(models.Call, {as:'OutgoingNumber', foreignKey: 'a_number_id'});
                    Number.hasMany(models.Call, {as:'IncomingNumber',foreignKey: 'b_number_id'});
                }
            },
            tableName: 'number'
        });
    return Number;
};
