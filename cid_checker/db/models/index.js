/**
 * Created by Armin Baljic on 5/5/2016.
 */

"use strict";

var db = {};
var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var ini = require('ini');
var util = require('util');
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var sequelize = new Sequelize(config.db.database, config.db.username, config.db.password, {
    host: config.db.host,
    dialect: 'mysql',
    pool: {
        max: config.db.connectionLimit,
        min: 0,
        idle: 10000
    },
    define: {
        timestamps: false
    }
});

//noinspection JSUnresolvedFunction,JSUnresolvedVariable
fs.readdirSync(__dirname).filter(function (file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
}).forEach(function (file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
});

Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;