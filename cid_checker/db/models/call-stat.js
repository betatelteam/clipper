/**
 * Created by Muris Agic on 08/24/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var CallStat = sequelize.define('CallStat', {
        call_status_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'CallStatus',
                key: 'id'
            }
        },
        count_number: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                CallStat.belongsTo(models.CallStatus, {foreignKey: 'call_status_id'});
            }
        },
        tableName: 'view_call_stat'
    });
    return CallStat;
};