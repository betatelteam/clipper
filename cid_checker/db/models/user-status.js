/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var UserStatus = sequelize.define('UserStatus', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        notice: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                //UserStatus.hasMany(models.User, {foreignKey: 'user_status_id'})
            }
        },
        tableName: 'user_status'
    });
    return UserStatus;
};