/**
 * Created by Muris Agic on 08/24/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var OperatorStat = sequelize.define('OperatorStat', {
        country_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        operator_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        count_correct: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        count_fraud: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        count_unsuccessful: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        }
    }, {
        tableName: 'view_operator_stat'
    });
    return OperatorStat;
};