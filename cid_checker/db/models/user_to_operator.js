/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var UserToOperator = sequelize.define('UserToOperator', {
            user_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            operator_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Operator',
                    key: 'id'
                }
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    UserToOperator.belongsTo(models.User, {foreignKey: 'user_id'});
                    UserToOperator.belongsTo(models.Operator, {foreignKey: 'operator_id'});
                }
            },
            tableName: 'user2operator'
        });
    UserToOperator.removeAttribute('id');
    return UserToOperator;
};
