/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var ScheduleSpool = sequelize.define('ScheduleSpool', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                type: DataTypes.INTEGER(10),
                allowNull: true,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            scheduler_id: {
                type: DataTypes.INTEGER(10),
                allowNull: true,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            start_time: {
                type: DataTypes.TIME,
                allowNull: false
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    ScheduleSpool.belongsTo(models.User, {foreignKey: 'scheduler_id'});
                }
            },
            tableName: 'schedule_spool'
        });
    return ScheduleSpool;
};
