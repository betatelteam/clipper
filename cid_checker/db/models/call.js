/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Call = sequelize.define('Call', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            a_number_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Number',
                    key: 'id'
                }
            },
            b_number_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Number',
                    key: 'id'
                }
            },
            user_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            time: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
            }

        },
        {
            classMethods: {
                associate: function (models) {
                    Call.belongsTo(models.Number, {as: 'SourceNumber', foreignKey: 'a_number_id'});
                    Call.belongsTo(models.Number, {as: 'DestinationNumber', foreignKey: 'b_number_id'});
                    Call.belongsTo(models.User, {foreignKey: 'user_id'});
                    Call.hasMany(models.CdrIncomingCall, {foreignKey: 'call_id'});
                    Call.hasMany(models.CdrOutgoingCall, {foreignKey: 'call_id'});
                }
            },
            tableName: 'call'
        });
    return Call;
};
