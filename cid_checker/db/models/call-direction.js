/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var CallDirection = sequelize.define('CallDirection', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        notice: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        tableName: 'call_direction'
    });
    return CallDirection;
};
