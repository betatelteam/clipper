/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define('User', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                type: DataTypes.STRING,
                allowNull: false
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING,
                allowNull: true
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            user_status_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'UserStatus',
                    key: 'id'
                }
            },
            default_language_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Language',
                    key: 'id'
                }
            }

        },
        {
            classMethods: {
                associate: function (models) {
                    User.belongsTo(models.Language, {foreignKey: 'default_language_id'});
                    User.belongsTo(models.UserStatus, {foreignKey: 'user_status_id'});
                    User.hasMany(models.UserToOperator, {foreignKey: 'user_id'});
                    User.hasMany(models.UserToRole, {foreignKey: 'user_id'});
                    User.hasMany(models.Scheduler, {foreignKey: 'user_id'});
                    User.belongsToMany(models.Role, {through: models.UserToRole, foreignKey: 'user_id'});
                    User.belongsToMany(models.Operator, {through: models.UserToOperator, foreignKey: 'user_id'});
                },
                checkLogin: function (username, password, models) {
                    return this.find({include: [{model: models.Role},{model:models.Language}],where: {username: username, password: password}})
                }
            },
            tableName: 'user'
        });
    return User;
};
