/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Language = sequelize.define('Language', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        notice: {
            type: DataTypes.STRING,
            allowNull: false
        },
        language_key: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                Language.hasMany(models.User, {foreignKey: 'default_language_id'})
            }
        },
        tableName: 'language'
    });
    return Language;
};