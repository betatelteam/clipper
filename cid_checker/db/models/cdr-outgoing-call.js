/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var CdrOutgoingCall = sequelize.define('CdrOutgoingCall', {
        uuid: {
            type: DataTypes.STRING,
            primary_key: true,
            allowNull: false
        },
        start_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        answer_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        end_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        duration: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '0'
        },
        billsec: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '0'
        },
        hangup_cause: {
            type: DataTypes.STRING,
            allowNull: true
        },
        call_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'Call',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
                CdrOutgoingCall.belongsTo(models.Call, {foreignKey: 'call_id'});
            }
        },
        tableName: 'cdr_outgoing_call'
    });
    CdrOutgoingCall.removeAttribute('id');
    return CdrOutgoingCall;
};
