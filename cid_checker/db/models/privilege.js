/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Privilege = sequelize.define('Privilege', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        notice: {
            type: DataTypes.STRING,
            allowNull: true
        },
        disabled: {
            type: DataTypes.INTEGER(1),
            allowNull: false
        },
        parent_id: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: 'Privilege',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
                Privilege.belongsTo(models.Privilege, {foreignKey: 'parent_id'})
            }
        },
        tableName: 'privilege'
    });
    return Privilege;
};
