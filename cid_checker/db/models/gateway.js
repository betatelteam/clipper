/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Gateway = sequelize.define('Gateway', {
            id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            location_id: {
                type: DataTypes.INTEGER(10),
                allowNull: false,
                references: {
                    model: 'Country',
                    key: 'id'
                }
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false
            },
            number_of_channels: {
                type: DataTypes.INTEGER(10),
                allowNull: false
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    Gateway.hasMany(models.Number, {foreignKey: 'gateway_id'});
                    Gateway.belongsTo(models.Country, {foreignKey: 'location_id'});
                }
            },
            tableName: 'gateway'
        });
    return Gateway;
};
