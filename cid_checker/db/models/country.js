/**
 * Created by Muris Agic on 08/16/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Country = sequelize.define('Country', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        common_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        formal_name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        capital: {
            type: DataTypes.STRING,
            allowNull: true
        },
        currency_code: {
            type: DataTypes.STRING,
            allowNull: true
        },
        currency_name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        itu_code: {
            type: DataTypes.INTEGER(25),
            allowNull: false
        },
        iso_2_name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        iso_3_name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        tld: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                Country.hasMany(models.Operator, {foreignKey: 'country_id'});
                Country.hasMany(models.Gateway, {foreignKey: 'location_id'});
            }
        },
        tableName: 'country'
    });
    return Country;
};
