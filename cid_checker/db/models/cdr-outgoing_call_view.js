/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var CdrOutgoingCallView = sequelize.define('CdrOutgoingCallView', {
        uuid: {
            type: DataTypes.STRING,
            primary_key: true,
            allowNull: false
        },
        start_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        answer_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        end_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        duration: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '0'
        },
        billsec: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '0'
        },
        hangup_cause: {
            type: DataTypes.STRING,
            allowNull: true
        },
        call_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'Call',
                key: 'id'
            }
        },
        call_status_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'CallStatus',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
                CdrOutgoingCallView.belongsTo(models.Call, {foreignKey: 'call_id'});
            }
        },
        tableName: 'view_cdr_outgoing_call'
    });
    CdrOutgoingCallView.removeAttribute('id');
    return CdrOutgoingCallView;
};
