/**
 * Created by Muris Agic on 08/24/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var OperatorMonthlyStat = sequelize.define('OperatorMonthlyStat', {
        month: {
            type: DataTypes.INTEGER(2),
            allowNull: false
        },
        count_correct: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        count_fraud: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        count_unsuccessful: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        }
    }, {
        tableName: 'view_operator_monthly_stat'
    });
    return OperatorMonthlyStat;
};