/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var Role = sequelize.define('Role', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        notice: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                Role.belongsToMany(models.User, {through: models.UserToRole, foreignKey: 'role_id'});
                Role.hasMany(models.UserToRole, {foreignKey: 'role_id'});
            }
        },
        tableName: 'role'
    });
    return Role;
};
