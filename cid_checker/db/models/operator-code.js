/**
 * Created by Muris Agic on 5/5/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var OperatorCode = sequelize.define('OperatorCode', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        prefix: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        operator_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'Operator',
                key: 'id'
            }
        },
        min_digit: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '6'
        },
        max_digit: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '7'
        },
        proper: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        }
    }, {
        classMethods: {
            associate: function (models) {
                OperatorCode.belongsTo(models.Operator, {foreignKey: 'operator_id'});
            }
        },
        tableName: 'operator_code'
    });
    return OperatorCode;
};
