/**
 * Created by Muris Agic on 08/09/2016.
 */

module.exports = function (sequelize, DataTypes) {
    var CdrIncomingCall = sequelize.define('CdrIncomingCall', {
        uuid: {
            type: DataTypes.STRING,
            primary_key: true,
            allowNull: false
        },
        a_number: {
            type: DataTypes.STRING,
            allowNull: true
        },
        start_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        answer_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        end_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        },
        duration: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '0'
        },
        billsec: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '0'
        },
        hangup_cause: {
            type: DataTypes.STRING,
            allowNull: true
        },
        call_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            references: {
                model: 'Call',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
                CdrIncomingCall.belongsTo(models.Call, {foreignKey: 'call_id'});
            }
        },
        tableName: 'cdr_incoming_call'
    });
    CdrIncomingCall.removeAttribute('id');
    return CdrIncomingCall;
};
