-- MySQL dump 10.13  Distrib 5.6.29-76.2, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: clipper
-- ------------------------------------------------------
-- Server version	5.6.29-76.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdr_freeswitch`
--

DROP TABLE IF EXISTS `cdr_freeswitch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr_freeswitch` (
  `caller_id_name` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `caller_id_number` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `destination_number` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `context` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `start_stamp` datetime DEFAULT NULL,
  `answer_stamp` datetime DEFAULT NULL,
  `end_stamp` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `billsec` int(11) DEFAULT NULL,
  `hangup_cause` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `uuid` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `bleg_uuid` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `accountcode` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `domain_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_freeswitch`
--

LOCK TABLES `cdr_freeswitch` WRITE;
/*!40000 ALTER TABLE `cdr_freeswitch` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr_freeswitch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr_table_a_leg_freeswitch`
--

DROP TABLE IF EXISTS `cdr_table_a_leg_freeswitch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr_table_a_leg_freeswitch` (
  `call_uuid` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `uuid` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `caller_id_number` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `destination_number` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `start_stamp` datetime DEFAULT NULL,
  `answer_stamp` datetime DEFAULT NULL,
  `end_stamp` datetime DEFAULT NULL,
  `duration` int(10) unsigned NOT NULL DEFAULT '0',
  `billsec` int(10) unsigned NOT NULL DEFAULT '0',
  `hangup_cause` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sip_call_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `network_ip` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `internal_ip` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `codec` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `direct_gateway` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `redirect_gateway` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `caller_id` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `tel_no` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `to_user` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sip_endpoint_disposition` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sip_current_application` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_table_a_leg_freeswitch`
--

LOCK TABLES `cdr_table_a_leg_freeswitch` WRITE;
/*!40000 ALTER TABLE `cdr_table_a_leg_freeswitch` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr_table_a_leg_freeswitch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr_table_b_leg_freeswitch`
--

DROP TABLE IF EXISTS `cdr_table_b_leg_freeswitch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr_table_b_leg_freeswitch` (
  `call_uuid` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `uuid` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `caller_id_number` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `destination_number` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `start_stamp` datetime DEFAULT NULL,
  `answer_stamp` datetime DEFAULT NULL,
  `end_stamp` datetime DEFAULT NULL,
  `duration` int(10) unsigned NOT NULL DEFAULT '0',
  `billsec` int(10) unsigned NOT NULL DEFAULT '0',
  `hangup_cause` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sip_call_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `network_ip` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `internal_ip` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `codec` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `direct_gateway` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `redirect_gateway` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `caller_id` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `tel_no` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `to_user` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sip_endpoint_disposition` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sip_current_application` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_table_b_leg_freeswitch`
--

LOCK TABLES `cdr_table_b_leg_freeswitch` WRITE;
/*!40000 ALTER TABLE `cdr_table_b_leg_freeswitch` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr_table_b_leg_freeswitch` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-09 11:28:38
