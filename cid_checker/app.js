/**
 * Created by Muris Agic on 08/09/2016.
 */
'use strict';

var express = require('express');
var esllib = require('./freeswitch/esl');
var Scheduler = require('./freeswitch/scheduler')
var operator_class = require('./freeswitch/operator')
var ini = require('ini')
var bodyParser = require('body-parser')
var models = require('./db/models')
var fs = require('fs') //File system library
var util = require('util')
var readline = require('readline')
var globals = require('./global')

var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

/** Setup Application */
var app = express();
var port = process.env.PORT || config.system.port;
var esl = new esllib(config);

// Arrays of country
var operatorsArray = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    next();
});

app.locals.config = config;

/**Routes*/
require('./routes/security')(app);
require('./routes/users')(app);
require('./routes/cdr')(app);
require('./routes/operators')(app);
require('./routes/countries')(app);
require('./routes/gateways')(app);
require('./routes/reports')(app);
require('./routes/scheduler')(app);

/** Start Application */
app.listen(port);

/** Start Freeswitch part */

// prompt
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

rl.on ('line', function(line) {
    switch (line) {
        case 'quit' : 
            console.log(`Don't quit`)
            done()
            break
        case 'scheduler' : 
            models.Scheduler.findAll({where: {active:1}}).then((schedules) => {
                console.log(`Scheduler\n`)
                schedules.map((schedule) => {
                    console.log(`---------------------------------\nid:${schedule.id}\nname:${schedule.name}\nstart:${schedule.start_time}\nend:${schedule.end_time}`)
                })
            })
            break
        case '?' :
            console.log(`Scheduler object: scheduler\nQuit from script: quit\nHelp: ?\n`)
            break
        default :
            console.log(`Unknown command ${line} type ? for help`)
    }
})

// populate country array
models.Operator.findAll({include: [{model: models.Country},{model: models.UserToOperator},{model:models.Number,where:{enabled:1},include:[{model:models.Gateway}]}]})
        .then((operators) => {
                    operators.map((operator) => {
                        operatorsArray.push(new operator_class(operator))
                    })
                    globals.scheduler = new Scheduler(operatorsArray, esl);
        });

