'use strict'

class Operator {
    constructor(structOperator) {
        this._operator = structOperator
        this._country = structOperator.Country
        this._numbers = structOperator.Numbers
    }

    show() {
        console.log(`Operator name: ${this._operator.name}`)
        console.log(this._country.common_name)
        this._numbers.map((number) => console.log(`Phone number: ${number.phone_number} gateway: ${number.Gateway.name}`))
    }
    

    getSpecNumber() {
        return 38631798252
    }

    get operator() { return this._operator }
    get UserToOperators() { return this._operator.UserToOperators }
    get Numbers() { return this._operator._numbers }
    get NumbersCount() { return this._numbers.length }
    get Country () { return this._operator._country }
    
    getRandomNumber() {
        let random_element = Math.floor(Math.random() * this._numbers.length)
        return this._numbers[random_element]
    }
}
module.exports = Operator

