'use strict'
var connector = require('../db/connector');
var events = require('events');

class Channel {
    constructor(chanid, coreuuid, direction, time, caller_id, destination_number, api_call) {
        this._id = chanid;
        this._callid = 0;
        this._coreid = coreuuid;
        this.timer = null;
        this._uepoch_created_time = time;
        this._duration = 0;
        this._billsec = 0;
        this._uepoch_hangup_time = 0;
        this._direction = direction;
        this._dtmf = [];
        this._state = 'initialize';
        this._cancelHangup = false
        this.listeners = new Map()
        this._caller_id_number = caller_id
        this._destination_number = destination_number
        this._apicall = api_call
    }

    isFunction(obj) {
        return typeof obj == 'function' || false
    }

    addListener(label, callback) {
        this.listeners.has(label) || this.listeners.set(label, [])
        this.listeners.get(label).push(callback)
    }


    removeListener(label, callback) {
        let listeners = this.listeners.get(label), index
        if (listeners && listeners.length) {
            index = listeners.reduce((i, listener, index) => {
                return (this.isFunction(listener) && listener === callback) ?
                    i = index :
                    i
            }, -1)

            if (index > -1) {
                listeners.splice(index, 1);
                this.listeners.set(label, listeners)
                return true
            }
        }
        return false
    }

    emit (label, args) {
        console.log(`Emit function for channel ${this._direction}`)
        let listener = this.listeners.get(label)
        if (listener && listener.length) {
            listener.map((listener_function) => {
                listener_function(args)
            })   
            return true
        }
        return false
    }

    set caller_id_name(value) { this._caller_id_name = value }
    set caller_id_number(value) { this._caller_id_number = value }
    set destination_number(value) { this._destination_number = value }
    set hostname(value) {this._hostname = value}
    set date_start_stamp(value) { this._date_start_stamp = value }
    set uepoch_created_time(value) { this._uepoch_created_time = value }
    set sip_full_contact(value) { this._sip_full_contact = value }
    set sip_full_from(value) { this._sip_full_from = value }
    set uepoch_hangup_time(value) { this._uepoch_hangup_time = value }
    set uepoch_answered_time(value) { this._uepoch_answered_time = value }
    set progress_time(value) { this._progress_time = value }
    set progress_media_time(value) { this._progress_media_time = value }
    set uepoch_transfer_time(value) { this._uepoch_transfer_time = value }
    set other_leg_id(value) { this._other_leg_id = value }
    set caller_context(value) {this._caller_context = value}
    set duration(value) {this._duration = value}
    set billsec(value) {this._billsec = value}
    set hangup_cause(value) {this._hangup_cause = value}
    set proto_specific_hangup_cause(value) {this._proto_specific_hangup_cause = value}
    set hangup_cause_q850(value) { this._hangup_cause_q850 = value }
    set sip_hangup_disposition(value) { this._sip_hangup_disposition= value }
    set sip_hangup_phrase(value) { this._sip_hangup_phrase = value }
    set uepoch_ring_start_time(value) { this._uepoch_ring_start_time = value }
    set answered_time(value) { this._answered_time= value }
    set progressmsec(value) {this._progressmsec = value}
    set progress_mediamsec(value) {this._progress_mediamsec = value}
    set waitmsec(value) {this._waitmsec = value}
    set last_app(value ) {this._last_app = value}
    set dtmf(value) { this._dtmf.push(value) }
    set state(value) { this._state = value }
    set sip_p_preffered_identity(value) { this._sip_p_preffered_identity = value }
    set sip_p_asserted_identity(value) { this._sip_p_asserted_identity = value }
    set direction(value) { this._direction = value }

    get direction() { return this._direction }
    get caller_id_number() { return this._caller_id_number }
    get destination_number() { return this._destination_number }
    get unique_id() { return this._unique_id }
    get uepoch_progress_time() { return this._uepoch_progress_time }
    get uepoch_progress_media_time() { return this._uepoch_progress_media_time }
    get state() {return this._state}
    get pdd() { return getPddTime() }
    get to_host() { return this._to_host }
    get from_host() { return this._from_host }
    get uuid() { return this._id }
    get start_stamp() { return this._date_start_stamp }
    get answer_stamp() { return this._uepoch_answered_time }
    get end_stamp() { return this._uepoch_hangup_time }
    get duration() { return this._duration }
    get billsec() { return this._billsec }
    get hangup_cause() { return this._hangup_cause }
    get ringing_time() { 
        return this.getRingingTime()
    }

    getPddTime() {
        if (this._uepoch_ring_start_time !== 0) {
            return (parseInt(this._uepoch_ring_start_time) - parseInt(this._uepoch_created_time))
        } else {
            return 0
        }
    }
    getRingingTime() {
        if (this._uepoch_answered_time === 0) { 
            return parseInt(this._uepoch_hangup_time) - parseInt(this._uepoch_ring_start_time) 
        }
        else { 
            return parseInt(this._uepoch_answered_time) - parseInt(this._uepoch_ring_start_time) 
        }
    }

    set_hangup_task_id(value, callback) { 
        this._hangup_task_id = value;
        this._cancelHangupCallback = callback;
        if (this._cancelHangup) { callback(value) } else { 
            //console.log(`Cancel hangup is FALSE for channel id ${this._id}!`) 
        }
    }

    hangup() {
        console.log(`Direction -> ${this._direction}`)
        this._state = 'hangup'
        this.emit('hangup', this)
    }

    force_hangup(hangup_reason) {
        this._apicall('uuid_kill', this._id + ' ' + hangup_reason)
        this._custom_cdr = 'force_hangup: ' + hangup_reason 
        this.hangup()
    }

    answer() {
        this._state = 'answer'
        this.emit('answer', this)
    }

    ring() {
        this.emit('ring', this)
        this._state = 'ring'
    }

    progress() {
        this._state = 'ring'
        this.emit('ring', this)
    }

    cancelHangup() {
        //console.log(`Channel ${this._id} cancelling hangup`);
        this._cancelHangup = true;
        if (typeof this._cancelHangupCallback !== 'undefined') { this._cancelHangupCallback(this._hangup_task_id) }
    }


    getJSON() {
        return {
            id: this._id,
            core_id: this._core_id,
            direction: this._direction,
            destination_number: this._destination_number,
            state: this._state
        }
    }

}

module.exports = Channel;
