'use strict'
var fs = require('fs');
var ini = require('ini');
var esl = require('modesl');
var util = require('util');
var globals = require('../global')
var Channel = require('./channel')
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var channels = []



class ESL {
    constructor(config) {
        this._fsw = null
        this.init(config.freeswitch.host, config.freeswitch.port, config.freeswitch.password)
    }

    getChannel(uniqueId) {
        return channels.find(channel => channel._id === uniqueId)
    }

    removeChannel(uniqueId) {
        let index = channels.findIndex(channel => channel._id === uniqueId)
        if (index !== -1)  {
            channels[index] = null
            channels.splice(index, 1)
        }
    }

    init(host, port, password) {
        this._fsw = new esl.Connection(host, port, password, function() {
            this.api('status', function(res) {
                console.log(res.getBody())
            })

            this.subscribe(['ALL'], function(){
                console.log('Subscribe on all events')
            })
        }) // new connection

        // event handler
        this._fsw.on('error', function(error) {
            switch(error.code) {
                case 'ECONNREFUSED' :
                    console.log('Error! Connection refused')
                    break
                case 'EADDRINUSE' :
                    console.log('Error! Connection refused')
                    break
                default:
                    console.log(error)
            }
        })

        this._fsw.on('esl::event::CHANNEL_ORIGINATE::*', evt => {
            console.log(`${this.getTime()} [${evt.getHeader('Caller-Caller-ID-Number')}] Channel originate to ${evt.getHeader('Caller-Destination-Number')}`)
        })

        this._fsw.on('esl::event::CHANNEL_ANSWER::*', evt => {
            console.log(`${this.getTime()} [${evt.getHeader('Caller-Caller-ID-Number')}] Channel answer`)
            let tempChannel = this.getChannel(evt.getHeader('Unique-ID'))
            if (typeof tempChannel !== 'undefined') {
                tempChannel.uuepoch_answered_time =  evt.getHeader('Caller-Channel-Anaswered-Time')
                tempChannel.answer()
            }
        })

        this._fsw.on('esl::event::CHANNEL_CREATE::*', evt => {
            // New channel init
            console.log(`${this.getTime()} [${evt.getHeader('Caller-Caller-ID-Number')}] Channel create to ${evt.getHeader('Caller-Destination-Number')} with channel direction ${evt.getHeader('Caller-Direction')}`)
            let channel_id = evt.getHeader('Unique-ID')
            let tempChannel = new Channel(channel_id, evt.getHeader('Core-UUID'), evt.getHeader('Caller-Direction'),evt.getHeader('Caller-Channel-Created-Time'), evt.getHeader('Caller-Caller-ID-Number'), evt.getHeader('Caller-Destination-Number'), this._api)
            tempChannel.destination_number = evt.getHeader('Caller-Destination-Number')
            tempChannel.date_start_stamp = new Date()
            tempChannel.direction = evt.getHeader('Caller-Direction')
            tempChannel.uepoch_answered_time = 0 

            console.log(`Direction of created channel ------------------------> ${tempChannel.direction}`)
            // Check scheduled calls for B-number match
            globals.scheduler.getScheduledCalls().map((scheduledcall) => {
                    if (tempChannel.direction === 'inbound' ) {
                            if (scheduledcall.b_number.phone_number.toString() === tempChannel.destination_number) {
                                console.log(`Found scheduled call inbound for number ${tempChannel.destination_number}`)
                                scheduledcall.in_channel(tempChannel)
                            }
                        } else {
                            if (scheduledcall.a_number.channel_number.toString() + '00' + scheduledcall.b_number.phone_number.toString() === tempChannel.destination_number) {
                                console.log(`Found scheduled call outbound for number ${tempChannel.destination_number}`)
                                scheduledcall.out_channel(tempChannel)
                            }
                        }
            })

            // Play ring on incoming channel and schedule hangup in 40 seconds random + 20 seconds fix
            if (evt.getHeader('Caller-Direction') === 'inbound') {
                // play ring
                this._fsw.api('uuid_ring_ready', channel_id, (res) => {
                    console.log(`${this.getTime()} [${evt.getHeader('Caller-Caller-ID-Number')}] Ring ready for ${evt.getHeader('Caller-Destination-Number')} `)
                });
                // Schedule hangup
                let hangup_timeout = Math.floor(Math.random() * 40000) + 20000
                this._fsw.api('sched_api', '+' + hangup_timeout + ' uuid_kill ' + tempChannel._id + ' ', (res) => { console.log(`Sched_hangup: channel ${evt.getHeader('Caller-Caller-ID-Number')} id: ${tempChannel._id}`) });
            }
            else {
                // Schedule hangup
                let hangup_timeout = 60000 // schedule hangup of outgoing channel to 1 minute
                this._fsw.api('sched_api', '+' + hangup_timeout + ' uuid_kill ' + tempChannel._id + ' ', (res) => { console.log(`Sched_hangup: ${evt.getHeader('Caller-Caller-ID-Number')} id: ${tempChannel._id}`) });
            }

            if (typeof evt.getHeader('variable_sip_h_Diversion') !== 'undefined' && evt.getHeader('Caller-Direction' === 'inbound')) {
                console.log(`[${evt.getHeader('Caller-Call-ID-Number')} SIP header diversion detected: ${evt.getHeader('variable_sip_h_Diversion')}`)
            }
            channels.push(tempChannel)

    })

        this._fsw.on('esl::event::CHANNEL_DESTROY::*', evt => {
            console.log(`${this.getTime()} [${evt.getHeader('Caller-Caller-ID-Number')}] Channel destroy to ${evt.getHeader('Caller-Destination-Number')} with channel direction ${evt.getHeader('Caller-Direction')}`)
            let tempChannel = this.getChannel(evt.getHeader('Unique-ID'));
            if (typeof tempChannel !== 'undefined') {
                tempChannel._uepoch_hangup_time = new Date()
                tempChannel.duration = evt.getHeader('variable_duration');
                tempChannel.billsec = evt.getHeader('variable_billsec');
                tempChannel._hangup_cause = evt.getHeader('Hangup-Cause');
                tempChannel.hangup()
            }

            // Check all scheduledcalls for destroy
            globals.scheduler.checkAllScheduledCalls();


            // remove channel from channels array
            this.removeChannel(evt.getHeader('Unique-ID'))
        })

        this._fsw.on('esl::event::CHANNEL_PROGRESS::*', evt =>  {
            console.log(`${this.getTime()} [${evt.getHeader('Caller-Caller-ID-Number')}] Channel progress to ${evt.getHeader('Caller-Destination-Number')} with channel direction ${evt.getHeader('Caller-Direction')}`)
            let tempChannel = this.getChannel(evt.getHeader('Unique-ID'));
            if (typeof tempChannel !== 'undefined') {
                tempChannel.progress_media_time = evt.getHeader('Caller-Channel-Progress-Media-Time');
                tempChannel.progress_time = evt.getHeader('Caller-Channel-Progress-Time')
                tempChannel.ring()
            }
        })
    } //init


    getTime() {
        return new Date().toISOString()
    }

}


module.exports = ESL
