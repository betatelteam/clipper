'use strict'
var country = require('./operator')
var models = require('../db/models')
var ScheduledCall = require('./scheduledcall')

class Scheduler {
    constructor(operators, esl) {
        this._operatorArray = operators
        this._scheduledCalls = []
        this._esl = esl
        this.refreshScheduledCalls()
        // read scheduled calls from database
    }

    refreshScheduledCalls() {
        // Find all active tasks that have today's date active
        models.Scheduler.findAll(
                { where: 
                    { 
                        scheduler_start: { $lt: models.Sequelize.fn('NOW') },
                        $or: [{scheduler_end: {$gt: models.Sequelize.fn('NOW')}},{scheduler_end: null}],
                        active: 1
                    }
                ,include: [{model: models.ScheduleSpool}]}
            )
            .bind(this)
            .then((schedules) => this.createSchedules(schedules))
        return 200;

        //TODO: delete all scheule spools in the past
    }

    changeStatus(schedule_id, new_status) {
        console.log(`Schedule id is: ${schedule_id} and new_status is ${new_status}`)
        return 200;
    }

    createSchedules(schedules) {
            // keep reference to function that should create Spool entry for each SpoolTask
            let that = this

            //Filter scheduler without created spool job
            //and create spool job in database
            schedules.filter((schedule) => schedule.ScheduleSpools.length == 0).map((schedule) => {
                that.createSingleScheduleSpool(schedule)
            })

            //Start call from cheduled events
            setTimeout(() => {
                schedules.filter((schedule) => schedule.ScheduleSpools.length > 0).map((schedule) => {
                    let curtimestamp = new Date().getTime() / 1000
                    let starttimestamp = new Date(schedule.ScheduleSpools[0].start_time).getTime()/1000
                    that.startRandomCall(schedule.ScheduleSpools[0].id, schedule.ScheduleSpools[0].user_id, starttimestamp.toFixed() - curtimestamp)
                })    
            }, 1000)
    }

    createSingleScheduleSpool(schedule) {
            let base_time = new Date()
            let start_time_parse = schedule.start_time.split(':')
            let end_time_parse = schedule.end_time.split(':')
            let interval_add = schedule.interval + Math.floor((schedule.interval/100) * schedule.delta_interval - 2 * Math.random() * ((schedule.interval/100) * schedule.delta_interval))
            let that = this
            start_time_parse = new Date(new Date().setHours(start_time_parse[0], start_time_parse[1], start_time_parse[2]))
            end_time_parse = new Date(new Date().setHours(end_time_parse[0], end_time_parse[1], end_time_parse[2]))
            let new_time = (base_time < start_time_parse)?new Date(start_time_parse.getTime() + interval_add * 60 * 1000):new Date(base_time.getTime() + interval_add * 60 * 1000)
            if (end_time_parse > start_time_parse && base_time < end_time_parse) {
                let tzoffset = new_time.getTimezoneOffset() * 60 * 1000
                models.ScheduleSpool.create({user_id: schedule.user_id,  scheduler_id: schedule.id, start_time: new Date(new_time - tzoffset).toISOString().slice(0, 19).replace('T', ' ')}).then((schedule_spool) => {
                    console.log(`Created new schedule spool ${schedule_spool}`)
                })
            }
    }

    getScheduledCalls() { 
        return this._scheduledCalls 
    }

    checkAllScheduledCalls() {
        let oldSize = this._scheduledCalls.length
        console.log(`{Size of scheduled calls is ${this._scheduledCalls.length}}`)
        this._scheduledCalls = this._scheduledCalls.filter((scheduled_call) => {
            return !scheduled_call.isFinished()
        }) 
        console.log(`{Size of new scheduled calls is ${this._scheduledCalls.length}}`)
        if (this._scheduledCalls.length < oldSize) {
            this.refreshScheduledCalls()
        }
    }


    startRandomCall(schedule_spool_id, userid, timer) {
        console.log(`start random call request schedule_spool_id: ${schedule_spool_id}, user_id: ${userid}, timer: ${timer}`)
        let outboundOperators = this._operatorArray.filter((operator) => {
            return operator.UserToOperators.filter((user2operator) => {
                return (user2operator.user_id == userid && user2operator.type == 'out')
            }).length > 0
        }).filter((operator_w_num) => { return operator_w_num.NumbersCount > 0 })


        let inboundOperators = this._operatorArray.filter((operator) => {
            return operator.UserToOperators.filter((user2operator) => {
                return (user2operator.user_id == userid && user2operator.type == 'in')
            }).length > 0
        }).filter((operator_w_num) => { return operator_w_num.NumbersCount > 0 })


        if (typeof inboundOperators === 'undefined' || typeof outboundOperators === 'undefined')  {
            console.log(`Error: For user ${userid} there is no inbound or outbound available operators`)
            return 0
        }
        if (outboundOperators.length == 0) { return 0 }
        //from array of outbound operators pick one random number
        let a_number  =  outboundOperators[Math.floor(Math.random() * outboundOperators.length)].getRandomNumber()
        //console.log(`Picked random operator ${outboundOperators.find(operator => operator._id = a_number.operator_id).name} and number ${a_number.phone_number}`)
        let b_number  =  inboundOperators[Math.floor(Math.random() * inboundOperators.length)].getRandomNumber()
        //console.log(`Picked random operator ${inboundOperators[b_number.operator_id].name} and number ${b_number.phone_number}`)

        if (timer === 'undefined') {timer = 5}

        // timer is in sec
        this._scheduledCalls.push(new ScheduledCall(schedule_spool_id, a_number, b_number, this._esl, timer, userid))
        return 200
    }
}

module.exports = Scheduler
