'use strict'
var models = require('../db/models')

class ScheduledCall {
    constructor(schedule_spool_id, a_number, b_number, esl, time, userid) {
        this._id = schedule_spool_id
        this._callid = 0
        this.a_number = a_number
        this.b_number = b_number
        this.gateway = a_number.Gateway.name
        this.prefix = a_number.channel_number
        this.esl = esl
        this.time = time
        this.userid = userid
        this.executed = false

        // convert timer from seconds to milliseconds
        console.log(`Created ScheduleCall in ${time} seconds`)
        this.timeout = setTimeout(() => {this.executeTimer()}, time * 1000)
    }

    executeTimer() {
        let that  = this
        console.log(`[${this.b_number.phone_number}] Originated call to ${this.b_number.phone_number} from gate ${this.a_number.Gateway.name} with prefix ${this.a_number.channel_number}`)
        models.Call.create({a_number_id: this.a_number.id, b_number_id: this.b_number.id, user_id: this.userid}).then(function(call) {
            that._callid = call.id
            that.esl._fsw.bgapi('originate', '{effective_caller_id_number=' + that.a_number.phone_number + ',origination_caller_id_name=\'' + that.a_number.phone_number + '\',origination_caller_id_number=' + that.a_number.phone_number + '}sofia/gateway/' + that.a_number.Gateway.name + '/' + that.a_number.channel_number + '00' + that.b_number.phone_number + ' &park()' , (status) => {})
            that.executed = true
        })
    }

    out_channel(value) {
        this._outchannel = value
        this._outchannel._callid = this._callid
        this._outchannel.addListener('hangup', this.outChannel_hangup_event)
        this._outchannel.addListener('ring', this.outChannel_ring_event)
    }
    in_channel(value) {
        this._inchannel = value
        this._inchannel._callid = this._callid
        this._inchannel.addListener('hangup', this.inChannel_hangup_event)
    }

    inChannel_hangup_event(context) {
        console.log(`scheduled call in channel hangup event`)
        models.CdrIncomingCall.create({uuid: context.uuid, a_number: context.caller_id_number, start_stamp: context.start_stamp, answer_stamp: context.answer_stamp, end_stamp: context.end_stamp, duration: context.duration, billsec: context.billsec, hangup_cause: context.hangup_cause, call_id: context._callid}).then((cdr_in_call) => {
        })
    }

    outChannel_hangup_event(context) {
        console.log(`scheduled call out channel hangup event`)
        models.CdrOutgoingCall.create({uuid: context.uuid, start_stamp: context.start_stamp, answer_stamp: context.answer_stamp, end_stamp: context.end_stamp, duration: context.duration, billsec: context.billsec, hangup_cause: context.hangup_cause, call_id: context._callid}).then((cdr_out_call) => {
        })
    }

    outChannel_ring_event(context) {
        console.log(`Outgoing channel callback function ring ${context._destination_number}`)
    }

    clearTimer() {
        clearTimeout(this.timeout)
    }

    isFinished() {
        if (this.executed && (!this._outchannel || (this._outchannel  && this._outchannel._state === 'hangup')) && (!this._inchannel || (this._inchannel && this._inchannel._state === 'hangup'))) {
            this._inchannel = null
            this._outchannel = null
            models.ScheduleSpool.destroy({where:{id:this._id}}).then((result) => {
                console.log(`Scheduled call with id ${this._id} is deleted from database with ${result}`)
            })
            return true
        } else {
            return false
        }
    }
}

module.exports = ScheduledCall

