/**
 * Created by Muris Agic on 2/20/2016.
 */
(function () {
    /**FILTER FOR ENCODING**/
    function encodingFilter($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    }

    function cutWord(value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                //Also remove . and , so its gives a cleaner result.
                if (value.charAt(lastspace - 1) == '.' || value.charAt(lastspace - 1) == ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    }

    /**
     * ADD CONTROLLERS TO MODULE
     */
    angular.module('clipper')
        .filter('encoding', encodingFilter)
        .filter('commacut', function () {
            return function (value, tail) {
                if (!value) return '';
                var indexOfComma = value.lastIndexOf(',');
                if (indexOfComma != -1) {
                    if (value.charAt(indexOfComma - 1) == '.' || value.charAt(indexOfComma - 1) == ',') {
                        indexOfComma = indexOfComma - 1;
                    }
                    value = value.substr(0, indexOfComma);
                    return value + (tail || ' …');
                }
                return value;
            };
        });
})();