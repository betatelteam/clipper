(function () {
    /** ROUTE CONFIGURATION */
    function routeConfig($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $authProvider, $httpProvider, $provide) {

        function redirectUnauthorized($q, $injector) {
            return {
                responseError: function (rejection) {
                    var $state = $injector.get('$state');
                    if (rejection.status === 401) {
                        localStorage.removeItem('user');
                        $state.go('login', {errorMessage: 'ERROR_UNAUTHORIZED_ACCESS'});
                    }
                    else
                        console.log("Error Code: " + rejection.status);
                    return $q.reject(rejection);
                }
            }
        }

        $provide.factory('redirectUnauthorized', redirectUnauthorized);

        $httpProvider.interceptors.push('redirectUnauthorized');

        $authProvider.loginUrl = 'http://localhost:9000/api/login';

        $urlRouterProvider.when('/', './index.html');

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');
            $state.go('login', null, {
                location: false
            });
        });

        $ocLazyLoadProvider.config({
            debug: false
        });

        $stateProvider
            .state('404', {
                templateUrl: "views/errors/error-404.html",
                data: {pageTitle: 'Dashboard', specialClass: 'gray-bg'}
            })
            .state('index', {
                url: "",
                abstract: true,
                templateUrl: "views/common/content.html"
            })
            .state('login', {
                url: "/login",
                templateUrl: "views/login/login.html",
                controller: 'AuthenticationController',
                data: {pageTitle: 'Login', specialClass: 'gray-bg'},
                params: {errorMessage: null}
            })
            .state('index.dashboard', {
                url: "/dashboard",
                controller: 'DashboardController',
                templateUrl: "views/dashboard/dashboard.html",
                data: {pageTitle: 'Dashboard'},
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                files: ['external/js/plugins/Chart.min.js']
                            },
                            {
                                name: 'chart.js',
                                files: ['external/js/plugins/Chart.min.js', 'external/js/angular/angular-chart.min.js']
                            }
                        ]);
                    }
                    ,
                    'acl': ['$injector', 'AclService', function ($injector, AclService) {
                        var $state = $injector.get('$state');
                        if (AclService.can('VIEW_DASHBOARD')) {
                            return true;
                        } else {
                            $state.go('404', {errorMessage: 'ERROR_UNAUTHORIZED_ACCESS'});
                        }
                    }]
                }
            })
            .state('index.users', {
                url: "/users",
                controller: 'UsersController',
                templateUrl: "views/administration/users/users.html",
                data: {pageTitle: 'Users'},
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                files: ['external/css/chosen.css', 'external/js/plugins/chosen.jquery.js', 'external/js/plugins/chosen.js']
                            }
                        ]);
                    },
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_USERS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.calls', {
                url: "/calls",
                controller: 'CdrController',
                templateUrl: "views/cdr/cdr-calls.html",
                data: {pageTitle: 'Calls'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_CALLS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.scheduler', {
                url: "/scheduler",
                controller: 'SchedulerController',
                templateUrl: "views/scheduler/scheduler.html",
                data: {pageTitle: 'Scheduler'},
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'ui.switchery',
                                files: ['external/css/switchery.css', 'external/js/plugins/switchery.js', 'external/js/plugins/ng-switchery.js']
                            },
                            {
                                files: ['external/css/awesome-bootstrap-checkbox.css']
                            },
                            {
                                name: 'mgcrea.ngStrap',
                                files: ['external/js/plugins/angular-strap.min.js']
                            }
                        ]);
                    },
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_SCHEDULER'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.scheduler-wizard', {
                url: "/scheduler-wizard",
                templateUrl: "views/scheduler/wizard/scheduler_wizard.html",
                data: {pageTitle: 'Scheduler - Wizard'},
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                files: ['external/css/jquery.steps.css']
                            },
                            {
                                name: 'ui.bootstrap.datetimepicker',
                                files: ['external/js/plugins/datetime-picker.js']
                            },
                            {
                                name: 'datePicker',
                                files: ['external/css/angular-datapicker.css', 'external/js/plugins/angular-datepicker.js']
                            },
                            {
                                files: ['external/css/clockpicker.css', 'external/js/plugins/clockpicker.js']
                            }
                        ]);
                    }
                }
            })
            .state('index.scheduler-wizard.general-page', {
                url: '/general-page',
                templateUrl: 'views/scheduler/wizard/scheduler_general_page.html',
                data: {pageTitle: 'Scheduler - Wizard'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('CREATE_TASK'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.scheduler-wizard.confirmation-page', {
                url: '/confirmation-page',
                templateUrl: 'views/scheduler/wizard/scheduler_confirm_page.html',
                data: {pageTitle: 'Scheduler - Wizard'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('CREATE_TASK'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.countries', {
                url: "/countries",
                controller: 'CountriesController',
                templateUrl: "views/administration/operators/countries.html",
                data: {pageTitle: 'Countries'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_COUNTRIES'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.operators', {
                url: "/operators",
                controller: 'OperatorsController',
                templateUrl: "views/administration/operators/operators.html",
                data: {pageTitle: 'Operators'},
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                files: ['external/css/checkbox.css']
                            }
                        ]);
                    },
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_OPERATORS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.gateways', {
                url: "/gateways",
                controller: 'GatewaysController',
                templateUrl: "views/administration/gateways/gateways.html",
                data: {pageTitle: 'Gateways'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_GATEWAYS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.management', {
                url: "/management",
                controller: 'ManagementController',
                templateUrl: "views/administration/management/management.html",
                data: {pageTitle: 'Management'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_MANAGEMENT'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.roles-privileges', {
                url: "/roles-privileges",
                controller: 'RolesPrivilegesController',
                templateUrl: "views/administration/roles_privileges/roles_privileges.html",
                data: {pageTitle: 'Roles and Privileges'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_ROLES_AND_PRIVILEGES'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.orders', {
                url: "/orders",
                controller: 'OrdersController',
                templateUrl: "views/orders/orders.html",
                data: {pageTitle: 'Orders'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_ORDERS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.tickets', {
                url: "/tickets",
                controller: 'TicketsController',
                templateUrl: "views/tickets/tickets.html",
                data: {pageTitle: 'Tickets'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_TICKETS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            }).state('index.ticketsDetail', {
                url: '/tickets/:id',
                data: {pageTitle: 'Tickets - Detail'},
                templateUrl: 'views/tickets/ticket-detail.html',
                controller: 'TicketsController',
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_TICKETS'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
            .state('index.configuration', {
                url: "/configuration",
                controller: 'ConfigurationController',
                templateUrl: "views/configuration/configuration.html",
                data: {pageTitle: 'Configuration'},
                resolve: {
                    'acl': ['$injector', 'AclService', function ($q, AclService) {
                        if (AclService.can('VIEW_CONFIGURATION'))
                            return true;
                        else
                            $q.reject('redirectUnauthorized');
                    }]
                }
            })
    }

    function paginationConfig(stConfig) {
        stConfig.pagination.template = 'views/components/pagination.html';
    }

    function notificationConfig(toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body',
            closeButton: true,
            debug: false,
            progressBar: false,
            onclick: null,
            showDuration: "400",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
        });
    }

    /**
     * ADD CONFIGURATIONS TO MODULE
     */
    angular.module('clipper')
        .config(routeConfig).run(function ($rootScope, $state) {
            $rootScope.$state = $state;
            $rootScope.$on('$stateChangeStart', function (event, toState) {
                var user = JSON.parse(localStorage.getItem('user'));
                if (user) {
                    $rootScope.authenticated = true;
                    $rootScope.currentUser = user;

                    /* User already logged in */
                    if (toState.name === "login") {
                        event.preventDefault();
                        $state.go('index.dashboard');
                    }
                }
            });
        })
        .config(paginationConfig)
        .config(notificationConfig)
        .config(['AclServiceProvider', function (AclServiceProvider) {
            var myConfig = {
                storage: 'localStorage',
                storageKey: 'AclService'
            };
            AclServiceProvider.config(myConfig);
        }]).config(['ChartJsProvider', function (ChartJsProvider) {
        ChartJsProvider.setOptions('doughnut', {
            colours: [
                {
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,1)",
                    pointColor: "rgba(26,179,148,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)"
                },
                {
                    fillColor: "rgba(169,68,86,0.5)",
                    strokeColor: "rgba(169,68,86,0.7)",
                    pointColor: "rgba(169,68,86,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(169,68,86,1)"
                },
                {
                    fillColor: "rgba(28,132,198,0.5)",
                    strokeColor: "rgba(28,132,198,0.7)",
                    pointColor: "rgba(28,132,198,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(28,132,198,1)"
                }
            ]
        });
    }]);
})();