(function () {

    /**
     * AUTHENTICATION CONTROLLER
     */
    function AuthenticationController($scope, $auth, toastr, $filter, $translate, $state, $rootScope, $stateParams, AclService, userService) {
        var translate = $filter('translate');
        $scope.desc = '';
        $scope.errorMessage = $stateParams.errorMessage;
        $scope.submitDisabled = false;

        $scope.can = AclService.can;

        $scope.login = function () {
            $scope.submitDisabled = true;

            var credentials = {
                username: $scope.username,
                password: $scope.password
            };

            $auth.login(credentials).then(function (response) {
                    if (response.status == 200) {
                        var user = JSON.stringify(response.data.user);
                        localStorage.setItem('user', user);

                        $rootScope.authenticated = true;
                        $rootScope.currentUser = response.data.user;

                        AclService.clearData();
                        if (!AclService.resume()) {
                            userService.getRoleProvider().query().$promise.then(function (roles) {
                                userService.getRolePrivilegeProvider().query().$promise.then(function (rolePrivileges) {
                                    var aclData = {};
                                    for (var i = 0; i < roles.length; i++) {
                                        var privileges = [];
                                        for (var j = 0; j < rolePrivileges.length; j++) {
                                            if (rolePrivileges[j].role_id == roles[i].id)
                                                privileges.push(rolePrivileges[j].privilege_id);
                                        }
                                        if (privileges.length > 0)
                                            aclData[roles[i].id] = privileges;
                                    }
                                    AclService.setAbilities(aclData);
                                });
                            });
                        }
                        for (var i = 0; i < response.data.user.Roles.length; i++)
                            AclService.attachRole(response.data.user.Roles[i].id);

                        $state.go('index.dashboard');
                        $translate.use(response.data.user.Language.language_key);
                        toastr.success(translate('WELCOME_MESSAGE'), $rootScope.currentUser.first_name + ' ' + $rootScope.currentUser.last_name);
                    }
                    $scope.submitDisabled = false;
                })
                .catch(function (error) {
                    if (error.status == 401)
                        $scope.errorMessage = 'ERROR_INVALID_LOGIN';
                    else if (error.status == 409)
                        $scope.errorMessage = 'ERROR_DISABLED_LOGIN';
                    else
                        $scope.errorMessage = error.statusText;
                    $scope.submitDisabled = false;
                })
        };

        $scope.logout = function () {
            $auth.logout().then(function () {
                localStorage.removeItem('user');
                $rootScope.authenticated = false;
                $rootScope.currentUser = null;
                $state.go('login');
            });
        };
    }

    /**
     *LANGUAGE CONTROLLER
     */
    function TranslateController($filter, $translate, $state, $scope, $rootScope, toastr, userService) {
        var translate = $filter('translate');
        $scope.changeLanguage = function (langKey) {
            var params = {language_key: langKey, user_id: $rootScope.currentUser.id};
            userService.getUserLanguageProvider().save(params).$promise.then(function (result) {
                if (result && result.code == 200) {
                    $translate.use(langKey);
                    $scope.language = langKey;
                    $state.reload();
                    toastr.success(translate('MSG_LANGUAGE_CHANGED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_LANGUAGE_CHANGED'));
            });
        }
    }

    /**
     *DASHBOARD CONTROLLER
     */
    function DashboardController($scope, $http, $rootScope, $filter, $modal, cdrService, operatorService, ticketService, reportService, AclService) {
        var translate = $filter('translate');
        $scope.can = AclService.can;
        $scope.data = [];
        $scope.operatorStat = [];
        $scope.country = null;
        $scope.colours = ["#1AB394", "#A94456", "#1C84C6"];
        $scope.statuses = [translate('CORRECT'), translate('FRAUD'), translate('UNSUCCESSFUL')];

        Date.prototype.getWeek = function () {
            var d = new Date(+this);
            d.setHours(0, 0, 0);
            d.setDate(d.getDate() + 4 - (d.getDay() || 7));
            return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
        };

        /**GET DATA AFTER CHANGING OPERATOR*/
        $scope.changeOperator = function (operator) {
            $scope.operator = operator;
            var params = getParameters();
            getMonthlyStat(params);
            getCallStat('date');
            getCallStat('week');
            getCallStat('month');
            getCallStat('year');
        };

        /**GET CALLS*/
        $scope.getCalls = function (type) {
            var params = getParameters(type);
            cdrService.calls = cdrService.getOutgoingCallProvider(type, params).query(params);
            $modal.open(callsModal);
        };

        /**CALL DATA BY STATUSES*/
        var getCallData = function (list) {
            var data = {correct: 0, fraud: 0, unsuccessful: 0};
            if (!list || list.length == 0) {
                data.hasData = false;
                return data;
            }
            for (var i = 0; i < list.length; i++) {
                if (list[i].call_status_id == 1)
                    data.correct = list[i].count_number;
                else if (list[i].call_status_id == 2)
                    data.fraud = list[i].count_number;
                else
                    data.unsuccessful = list[i].count_number;
            }
            data.hasData = true;
            return data;
        };

        /**SEARCH PARAMETERS*/
        var getParameters = function (type) {
            var exist = false;
            var params = {};
            var data = {
                user_id: $rootScope.currentUser.id,
                date: moment(new Date()).format('DD-MM-YYYY'),
                year: moment(new Date()).format('YYYY'),
                week: (new Date()).getWeek(),
                month: moment(new Date()).format('MM'),
                operator_id: ($scope.operator ? $scope.operator.id : null)
            };
            if (type)
                params[type] = data[type];
            $rootScope.currentUser.Roles.forEach(function (role) {
                if (role.id == 1)
                    exist = true;
            });
            if (!exist)
                params['user_id'] = data['user_id'];
            if ($scope.operator)
                params['operator_id'] = data['operator_id'];
            if (type == 'week' || type == 'month')
                params['year'] = data['year'];
            return params;
        };

        /**CALL STAT*/
        var getCallStat = function (type) {
            var params = getParameters(type);
            reportService.getCallReportProvider(type, params).query(params)
                .$promise.then(function (stat) {
                var pieData = getCallData(stat);
                $scope.data[type] = {
                    hasData: pieData.hasData,
                    values: [pieData.correct, pieData.fraud, pieData.unsuccessful]
                }
            });
            $scope.operatorStat[type] = reportService.getOperatorReportProvider(type, params).query(params);
        };

        /**MONTHLY STAT*/
        var getMonthlyStat = function (params) {
            reportService.getOperatorMonthlyReportProvider(params).query(params)
                .$promise.then(function (stat) {
                var currentDate = new Date();
                var months = [translate('JANUARY'), translate('FEBRUARY'), translate('MARCH'), translate('APRIL'), translate('MAY'), translate('JUNE'),
                    translate('JULY'), translate('AUGUST'), translate('SEPTEMBER'), translate('OCTOBER'), translate('NOVEMBER'), translate('DECEMBER')];
                var correctValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var fraudValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var unsuccessfulValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var labels = [];
                var dataCorrect = [];
                var dataFraud = [];
                var dataUnsuccessful = [];
                if (stat && stat.length > 0)
                    stat.forEach(function (item) {
                        correctValues[item.month - 1] = item.count_correct;
                        fraudValues[item.month - 1] = item.count_fraud;
                        unsuccessfulValues[item.month - 1] = item.count_unsuccessful;
                    });
                for (var i = currentDate.getMonth() + 1; i < months.length; i++) {
                    labels.push(months[i]);
                    dataCorrect.push(correctValues[i]);
                    dataFraud.push(fraudValues[i]);
                    dataUnsuccessful.push(unsuccessfulValues[i]);
                }

                for (var j = 0; j <= currentDate.getMonth(); j++) {
                    labels.push(months[j]);
                    dataCorrect.push(correctValues[j]);
                    dataFraud.push(fraudValues[j]);
                    dataUnsuccessful.push(unsuccessfulValues[j]);
                }
                $scope.data = {
                    labels: labels,
                    values: [dataCorrect, dataFraud, dataUnsuccessful
                    ]
                };

                $scope.onClick = function (points, evt) {
                    console.log(points, evt);
                };
            });
        };

        /**LOAD DASHBOARD DATA*/
        var loadData = function () {
            var params = getParameters();
            operatorService.getOperatorInboundProvider(params).query(params).$promise.then(function (operators) {
                $scope.operators = operators;
                if (operators.length == 1)
                    $scope.operator = operators[0];
            });
            getMonthlyStat(params);
            getCallStat('date');
            getCallStat('week');
            getCallStat('month');
            getCallStat('year');
        };
        loadData();

        /**MODAL PANELS*/
        var callsModal = {
            templateUrl: 'views/cdr/modals/cdr-calls-modal.html',
            backdrop: 'static',
            windowClass: 'app-modal-window',
            controller: CdrActionsController
        };
    }

    function DashboardActionsController($scope, $uibModalInstance, cdrService) {
        $scope.itemsByPage = 5;
        $scope.calls = cdrService.getCalls();

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *USERS CONTROLLER
     */
    function UsersController($scope, $modal, userService, toastr, $filter, AclService) {
        var translate = $filter('translate');
        $scope.itemsByPage = 15;
        $scope.rowCollection = userService.getUserProvider().query();
        $scope.userStatuses = userService.getStatusProvider().query();
        $scope.can = AclService.can;

        var modal = {
            templateUrl: 'views/administration/users/modals/user-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: UsersActionsController
        };

        var confirmModal = {
            templateUrl: 'views/administration/users/modals/user-confirm-modal.html',
            backdrop: 'static',
            controller: UsersActionsController
        };

        $scope.createUser = function () {
            userService.actionType = 'create';
            userService.user = {};
            $modal.open(modal);
        };

        $scope.updateUser = function (user) {
            userService.actionType = 'update';
            userService.user = user;
            $modal.open(modal);
        };

        $scope.lockUser = function (user) {
            userService.actionType = 'lock';
            userService.user = user;
            $modal.open(confirmModal);
        };

        $scope.unlockUser = function (user) {
            userService.actionType = 'unlock';
            userService.user = user;
            $modal.open(confirmModal);
        };

        userService.handleCreate = function (user, $uibModalInstance, status) {
            status.flag = true;
            userService.getUserProvider().save(user).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    $scope.rowCollection.unshift(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_USER_CREATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_USER_CREATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_USER_CREATE'));
            });
        };

        userService.handleUpdate = function (user, $uibModalInstance, status) {
            status.flag = true;
            userService.getUserProvider().save(user).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_USER_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_USER_UPDATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_USER_UPDATE'));
            });
        };

        function updateTableRow(updatedItem) {
            for (var i = 0; i < $scope.rowCollection.length; i++) {
                if ($scope.rowCollection[i].id == updatedItem.id) {
                    angular.extend($scope.rowCollection[i], updatedItem);
                    break;
                }
            }
        }
    }

    function UsersActionsController($scope, $uibModalInstance, userService) {
        if (userService.actionType == 'create' || userService.actionType == 'update') {
            $scope.languages = userService.getLanguageProvider().query();
            $scope.roles = userService.getRoleProvider().query();
        }
        $scope.status = {flag: false};
        $scope.user = userService.getUser();
        $scope.user.confirm_password = $scope.user.password;
        $scope.actionTypeLabel = userService.actionType == 'create' ? 'ADD' : 'EDIT';
        $scope.selectedRoles = $scope.user.Roles;

        $scope.ok = function () {
            if (userService.actionType == 'lock') {
                $scope.user.user_status_id = 2;
                userService.handleUpdate($scope.user, $uibModalInstance, $scope.status);
            }
            else if (userService.actionType == 'unlock') {
                $scope.user.user_status_id = 1;
                userService.handleUpdate($scope.user, $uibModalInstance, $scope.status);
            }
            else {
                if ($scope.userForm.$valid) {
                    if (userService.actionType == 'create') {
                        $scope.user.user_status_id = 1;
                        $scope.user.Roles = $scope.selectedRoles;
                        userService.handleCreate($scope.user, $uibModalInstance, $scope.status);
                    }
                    else if (userService.actionType == 'update')
                        $scope.user.Roles = $scope.selectedRoles;
                    userService.handleUpdate($scope.user, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.userForm.submitted = true;
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *OPERATORS CONTROLLER
     */
    function CdrController($scope, $rootScope, cdrService, $uibModal, AclService) {
        $scope.itemsByPage = 30;
        $scope.callStatuses = cdrService.getStatusProvider().query();
        $scope.expanded = [];
        $scope.incomingCalls = [];
        $scope.disabledCall = false;
        $scope.can = AclService.can;

        var loadCalls = function () {
            var params = {};
            var exist = false;
            $rootScope.currentUser.Roles.forEach(function (role) {
                if (role.id == 1) {
                    exist = true;
                }
            });
            if (!exist)
                params['user_id'] = $rootScope.currentUser.id;
            $scope.rowCollection = cdrService.getOutgoingCallProvider('all', params).query(params);
        };
        loadCalls();

        $scope.expand = function (outgoingCall) {
            var exist = false;
            $scope.expanded[outgoingCall.uuid] = !$scope.expanded[outgoingCall.uuid];
            if (!exist)
                $scope.incomingCalls[outgoingCall.uuid] = cdrService.getIncomingCallProvider().query({call_id: outgoingCall.call_id});
        };

        $scope.isExpanded = function (uuid) {
            return $scope.expanded[uuid];
        };

        $scope.call = function () {
            $uibModal.open(newCallModal);
        };

        $scope.isFraud = function (outgoingCall, incomingCalls) {
            outgoingCall.call_status_id = 1;
            if (!incomingCalls) return false;
            incomingCalls.forEach(function (incomingCall) {
                incomingCall.call_status_id = (incomingCall.a_number != outgoingCall.Call.SourceNumber.phone_number) + 1;
                if (incomingCall.call_status_id == 2)
                    outgoingCall.call_status_id = 2;
            });
            return outgoingCall.call_status_id == 2;
        };

        var confirmModal = {
            templateUrl: 'views/cdr/modals/cdr-confirm-modal.html',
            backdrop: 'static',
            controller: CdrActionsController
        };

        var newCallModal = {
            templateUrl: 'views/cdr/modals/cdr-new-call-modal.html',
            backdrop: 'static',
            controller: CdrActionsController
        };

        var callInProgressModal = {
            templateUrl: 'views/cdr/modals/cdr-call-in-progress-modal.html',
            backdrop: 'static',
            controller: CallActionsController
        };

        cdrService.callInProgress = function ($uibModalInstance) {
            $uibModalInstance.close();
            $uibModal.open(callInProgressModal);
        };
    }

    function CdrActionsController($scope, $rootScope, $uibModalInstance, cdrService) {
        $scope.itemsByPage = 5;
        $scope.calls = cdrService.getCalls();
        $scope.callStatuses = cdrService.getStatusProvider().query();
        $scope.expanded = [];
        $scope.incomingCalls = [];

        $scope.expand = function (outgoingCall) {
            var exist = false;
            $scope.expanded[outgoingCall.uuid] = !$scope.expanded[outgoingCall.uuid];
            if (!exist)
                $scope.incomingCalls[outgoingCall.uuid] = cdrService.getIncomingCallProvider().query({call_id: outgoingCall.call_id});
        };

        $scope.isExpanded = function (uuid) {
            return $scope.expanded[uuid];
        };

        $scope.ok = function () {
            cdrService.makeCall().save($rootScope.currentUser);
            cdrService.callInProgress($uibModalInstance);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function CallActionsController($scope, $uibModalInstance, $interval) {
        $scope.progressValue = 60;

        $interval(function () {
            $scope.progressValue--;
            if ($scope.progressValue == 0)
                $uibModalInstance.close();
        }, 1000, 60);
    }

    /**
     *COUNTRIES CONTROLLER
     */
    function CountriesController($scope, $modal, countryService, toastr, $filter, AclService) {
        var translate = $filter('translate');
        $scope.itemsByPage = 15;
        $scope.rowCollection = countryService.getCountryProvider().query();
        $scope.can = AclService.can;

        var modal = {
            templateUrl: 'views/administration/operators/modals/country-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: CountryActionsController
        };

        var confirmModal = {
            templateUrl: 'views/administration/operators/modals/country-confirm.html',
            backdrop: 'static',
            controller: CountryActionsController
        };

        $scope.createCountry = function () {
            countryService.actionType = 'create';
            countryService.country = {};
            $modal.open(modal);
        };

        $scope.updateCountry = function (country) {
            countryService.actionType = 'update';
            countryService.country = country;
            $modal.open(modal);
        };

        $scope.deleteCountry = function (country) {
            countryService.actionType = 'delete';
            countryService.country = country;
            $modal.open(confirmModal);
        };

        countryService.handleCreate = function (country, $uibModalInstance, status) {
            status.flag = true;
            countryService.getCountryProvider().save(country).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    $scope.rowCollection.unshift(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_COUNTRY_CREATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_COUNTRY_CREATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_COUNTRY_CREATE'));
            });
        };

        countryService.handleUpdate = function (country, $uibModalInstance, status) {
            status.flag = true;
            countryService.getCountryProvider().save(country).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_COUNTRY_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_COUNTRY_UPDATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_COUNTRY_CODE_UPDATE'));
            });
        };

        countryService.handleDelete = function (country, $uibModalInstance, status) {
            status.flag = true;
            countryService.getCountryProvider().delete({id: country.id}).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    removeTableRow(country);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_COUNTRY_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_COUNTRY_DELETE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_COUNTRY_DELETE'));
            });
        };

        function updateTableRow(updatedItem) {
            for (var i = 0; i < $scope.rowCollection.length; i++) {
                if ($scope.rowCollection[i].id == updatedItem.id) {
                    angular.extend($scope.rowCollection[i], updatedItem);
                    break;
                }
            }
        }

        function removeTableRow(item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.rowCollection.length; i++)
                    if ($scope.rowCollection[i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.rowCollection.splice(index, 1);
            }
        }
    }

    function CountryActionsController($scope, $uibModalInstance, countryService) {
        $scope.status = {flag: false};
        $scope.country = countryService.getCountry();
        $scope.actionTypeLabel = countryService.actionType == 'create' ? 'ADD' : 'EDIT';

        $scope.ok = function () {
            if (countryService.actionType == 'delete')
                countryService.handleDelete($scope.country, $uibModalInstance, $scope.status);
            else {
                if ($scope.countryForm.$valid) {
                    if (countryService.actionType == 'create')
                        countryService.handleCreate($scope.country, $uibModalInstance, $scope.status);
                    else if (countryService.actionType == 'update')
                        countryService.handleUpdate($scope.country, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.countryForm.submitted = true;
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *NUMBERS CONTROLLER
     */
    function NumbersController($scope, $modal, gatewayService, toastr, $filter, AclService) {
        var translate = $filter('translate');
        $scope.can = AclService.can;

        var modal = {
            templateUrl: 'views/administration/gateways/modals/number-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: NumbersActionsController,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['external/css/awesome-bootstrap-checkbox.css']
                        }
                    ]);
                }
            }
        };

        var confirmModal = {
            templateUrl: 'views/administration/gateways/modals/number-confirm-modal.html',
            backdrop: 'static',
            controller: NumbersActionsController
        };

        $scope.createNumber = function (gateway) {
            gatewayService.actionType = 'create';
            gatewayService.number = {};
            gatewayService.number.gateway_id = gateway.id;
            gatewayService.number.enabled = 1;
            gatewayService.number.Gateway = gateway;
            $modal.open(modal);
        };

        $scope.updateNumber = function (number) {
            gatewayService.actionType = 'update';
            gatewayService.number = number;
            $modal.open(modal);
        };

        $scope.deleteNumber = function (number) {
            gatewayService.actionType = 'delete';
            gatewayService.number = number;
            $modal.open(confirmModal);
        };

        $scope.enableNumber = function (number) {
            gatewayService.actionType = 'enable';
            gatewayService.number = number;
            $modal.open(confirmModal);
        };

        $scope.disableNumber = function (number) {
            gatewayService.actionType = 'disable';
            gatewayService.number = number;
            $modal.open(confirmModal);
        };

        gatewayService.handleCreateNumber = function (number, $uibModalInstance, status) {
            status.flag = true;
            gatewayService.getNumberProvider().save(number).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    gatewayService.addTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_NUMBER_CREATED'));
                }
                else
                    toastr.error(translate((result.name && result.name == 'SequelizeUniqueConstraintError') ?
                        'MSG_ERROR_NUMBER_ALREADY_EXISTS' : 'MSG_ERROR_NUMBER_CREATE'));

            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_CODE_CREATE'));
            });
        };

        gatewayService.handleUpdateNumber = function (number, $uibModalInstance, status) {
            status.flag = true;
            gatewayService.getNumberProvider().save(number).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    gatewayService.updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_NUMBER_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_NUMBER_UPDATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_NUMBER_UPDATE'));
            });
        };

        gatewayService.handleDeleteNumber = function (number, $uibModalInstance, status) {
            status.flag = true;
            gatewayService.getNumberProvider().delete({id: number.id}).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    gatewayService.removeTableRow(number);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_NUMBER_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_NUMBER_DELETE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_NUMBER_DELETE'));
            });
        };
    }

    function NumbersActionsController($scope, $uibModalInstance, gatewayService, operatorService) {
        $scope.status = {flag: false};
        $scope.actionType = gatewayService.actionType;
        $scope.number = gatewayService.getNumber();
        $scope.actionTypeLabel = gatewayService.actionType == 'create' ? 'ADD' : 'EDIT';

        if (gatewayService.actionType == 'create' || gatewayService.actionType == 'update') {
            $scope.operators = operatorService.getOperatorProvider().query();
            $scope.gateways = gatewayService.getGatewayProvider().query();
            $scope.channel_numbers = gatewayService.getAvailableChannels($scope.number, gatewayService.actionType);
        }

        $scope.ok = function () {
            if (gatewayService.actionType == 'enable') {
                $scope.number.enabled = 1;
                gatewayService.handleUpdateNumber($scope.number, $uibModalInstance, $scope.status);
            }
            else if (gatewayService.actionType == 'disable') {
                $scope.number.enabled = 0;
                gatewayService.handleUpdateNumber($scope.number, $uibModalInstance, $scope.status);
            }
            else if (gatewayService.actionType == 'delete')
                gatewayService.handleDeleteNumber($scope.number, $uibModalInstance, $scope.status);
            else {
                if ($scope.numberForm.$valid) {
                    if (gatewayService.actionType == 'create')
                        gatewayService.handleCreateNumber($scope.number, $uibModalInstance, $scope.status);
                    else if (gatewayService.actionType == 'update')
                        gatewayService.handleUpdateNumber($scope.number, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.numberForm.submitted = true;
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *OPERATOR CODES CONTROLLER
     */
    function OperatorCodesController($scope, $modal, operatorService, toastr, $filter) {
        var translate = $filter('translate');
        var modal = {
            templateUrl: 'views/administration/operators/modals/operator-codes-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: OperatorCodesActionsController
        };

        var confirmModal = {
            templateUrl: 'views/administration/operators/modals/operator-codes-confirm.html',
            backdrop: 'static',
            controller: OperatorCodesActionsController
        };

        $scope.createOperatorCode = function (operatorId) {
            operatorService.actionType = 'create';
            operatorService.operatorCode = {};
            operatorService.operatorCode.operator_id = operatorId;
            $modal.open(modal);
        };

        $scope.updateOperatorCode = function (code) {
            operatorService.actionType = 'update';
            operatorService.operatorCode = code;
            $modal.open(modal);
        };

        $scope.deleteOperatorCode = function (code) {
            operatorService.actionType = 'delete';
            operatorService.operatorCode = code;
            $modal.open(confirmModal);
        };

        operatorService.handleCreateCode = function (code, $uibModalInstance, status) {
            status.flag = true;
            operatorService.getOperatorCodeProvider().save(code).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    operatorService.addTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_OPERATOR_CODE_CREATED'));
                }
                else
                    toastr.error(ranslate('MSG_ERROR_OPERATOR_CODE_CREATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_CODE_CREATE'));
            });
        };

        operatorService.handleUpdateCode = function (code, $uibModalInstance, status) {
            status.flag = true;
            operatorService.getOperatorCodeProvider().save(code).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    operatorService.updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_OPERATOR_CODE_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_OPERATOR_CODE_UPDATE'));

            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_CODE_UPDATE'));
            });
        };

        operatorService.handleDeleteCode = function (code, $uibModalInstance, status) {
            status.flag = true;
            operatorService.getOperatorCodeProvider().delete({id: code.id}).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    operatorService.removeTableRow(code);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_OPERATOR_CODE_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_OPERATOR_CODE_DELETE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_CODE_DELETE'));
            });
        };
    }

    function OperatorCodesActionsController($scope, $uibModalInstance, operatorService) {
        if (operatorService.actionType == 'create' || operatorService.actionType == 'update')
            $scope.operators = operatorService.getOperatorProvider().query();

        $scope.status = {flag: false};
        $scope.operatorCode = operatorService.getOperatorCode();
        $scope.actionTypeLabel = operatorService.actionType == 'create' ? 'ADD' : 'EDIT';

        $scope.ok = function () {
            if (operatorService.actionType == 'delete')
                operatorService.handleDeleteCode($scope.operatorCode, $uibModalInstance, $scope.status);
            else {
                if ($scope.operatorCodeForm.$valid) {
                    if (operatorService.actionType == 'create')
                        operatorService.handleCreateCode($scope.operatorCode, $uibModalInstance, $scope.status);
                    else if (operatorService.actionType == 'update')
                        operatorService.handleUpdateCode($scope.operatorCode, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.operatorCodeForm.submitted = true;
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *OPERATORS CONTROLLER
     */
    function OperatorsController($scope, $modal, operatorService, toastr, $filter) {
        var translate = $filter('translate');
        $scope.itemsByPage = 15;
        $scope.rowCollection = operatorService.getOperatorProvider().query();
        $scope.expanded = [];
        $scope.operatorCodes = [];

        $scope.expand = function (operatorId) {
            var exist = false;
            $scope.expanded.forEach(function (item) {
                if (item.id == operatorId) {
                    exist = true;
                    item.expand = !item.expand;
                    if (!item.expand && $scope.operatorCodes[operatorId])
                        delete $scope.operatorCodes[operatorId];
                }
            });
            if (!exist)
                $scope.expanded.push({id: operatorId, expand: true});
            $scope.operatorCodes[operatorId] = operatorService.getOperatorCodeProvider().query({operatorId: operatorId});
        };

        $scope.isExpanded = function (operatorId) {
            var expanded = false;
            if (!$scope.expanded) return false;
            $scope.expanded.forEach(function (item) {
                if (item.id == operatorId)
                    expanded = item.expand;
            });
            return expanded;
        };

        /**MODALS*/
        /**CREATE/UPDATE OPERATOR MODAL*/
        var modal = {
            templateUrl: 'views/administration/operators/modals/operator-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: OperatorsActionsController
        };

        /**CONFIRMATION MODAL*/
        var confirmModal = {
            templateUrl: 'views/administration/operators/modals/operator-confirm.html',
            backdrop: 'static',
            controller: OperatorsActionsController
        };

        /**ACTIONS - PREPARE*/
        /**CREATE OPERATOR*/
        $scope.createOperator = function () {
            operatorService.actionType = 'create';
            operatorService.operator = {};
            $modal.open(modal);
        };

        /**UPDATE OPERATOR*/
        $scope.updateOperator = function (operator) {
            operatorService.actionType = 'update';
            operatorService.operator = operator;
            $modal.open(modal);
        };

        /**DELETE OPERATOR*/
        $scope.deleteOperator = function (operator) {
            operatorService.actionType = 'delete';
            operatorService.operator = operator;
            $modal.open(confirmModal);
        };

        /**ACTIONS*/
        /**CREATE OPERATOR*/
        operatorService.handleCreate = function (operator, $uibModalInstance, status) {
            status.flag = true;
            operatorService.getOperatorProvider().save(operator).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    $scope.rowCollection.push(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_OPERATOR_CREATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_OPERATOR_CREATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_CREATE'));
            });
        };

        /**UPDATE OPERATOR*/
        operatorService.handleUpdate = function (user, $uibModalInstance, status) {
            status.flag = true;
            operatorService.getOperatorProvider().save(user).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_OPERATOR_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_OPERATOR_UPDATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_UPDATE'));
            });
        };

        /**DELETE OPERATOR*/
        operatorService.handleDelete = function (operator, $uibModalInstance, status) {
            status.flag = true;
            operatorService.getOperatorProvider().delete({id: operator.id}).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    removeTableRow(operator);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_OPERATOR_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_OPERATOR_DELETE'));

            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_OPERATOR_DELETE'));
            });
        };

        function updateTableRow(updatedItem) {
            for (var i = 0; i < $scope.rowCollection.length; i++) {
                if ($scope.rowCollection[i].id == updatedItem.id) {
                    angular.extend($scope.rowCollection[i], updatedItem);
                    break;
                }
            }
        }

        function removeTableRow(item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.rowCollection.length; i++)
                    if ($scope.rowCollection[i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.rowCollection.splice(index, 1);
            }
        }

        operatorService.addTableRow = function (item) {
            $scope.operatorCodes[item.operator_id].push(item);
        };

        operatorService.updateTableRow = function (updatedItem) {
            for (var i = 0; i < $scope.operatorCodes[updatedItem.operator_id].length; i++) {
                if ($scope.operatorCodes[updatedItem.operator_id][i].id == updatedItem.id) {
                    angular.extend($scope.operatorCodes[updatedItem.operator_id][i], updatedItem);
                    break;
                }
            }
        };

        operatorService.removeTableRow = function (item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.operatorCodes[item.operator_id].length; i++)
                    if ($scope.operatorCodes[item.operator_id][i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.operatorCodes[item.operator_id].splice(index, 1);
            }
        }
    }

    function OperatorsActionsController($scope, $uibModalInstance, operatorService, countryService) {
        if (operatorService.actionType == 'create' || operatorService.actionType == 'update')
            $scope.countries = countryService.getCountryProvider().query();

        $scope.status = {flag: false};
        $scope.operator = operatorService.getOperator();
        $scope.actionTypeLabel = operatorService.actionType == 'create' ? 'ADD' : 'EDIT';

        $scope.ok = function () {
            if (operatorService.actionType == 'delete')
                operatorService.handleDelete($scope.operator, $uibModalInstance, $scope.status);
            else {
                if ($scope.operatorForm.$valid) {
                    if (operatorService.actionType == 'create')
                        operatorService.handleCreate($scope.operator, $uibModalInstance, $scope.status);
                    else if (operatorService.actionType == 'update')
                        operatorService.handleUpdate($scope.operator, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.operatorForm.submitted = true;
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *GATEWAYS CONTROLLER
     */
    function GatewaysController($scope, $modal, gatewayService, toastr, $filter, AclService) {
        var translate = $filter('translate');
        $scope.itemsByPage = 15;
        $scope.rowCollection = gatewayService.getGatewayProvider().query();
        $scope.expanded = [];
        $scope.numbers = [];
        $scope.can = AclService.can;

        /*
         * MODAL PANELS
         */
        var modal = {
            templateUrl: 'views/administration/gateways/modals/gateway-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: GatewaysActionsController
        };

        var confirmModal = {
            templateUrl: 'views/administration/gateways/modals/gateway-confirm-modal.html',
            backdrop: 'static',
            controller: GatewaysActionsController
        };

        /*CREATE*/
        $scope.createGateway = function () {
            gatewayService.actionType = 'create';
            gatewayService.gateway = {};
            $modal.open(modal);
        };

        /*EDIT*/
        $scope.updateGateway = function (gateway) {
            gatewayService.actionType = 'update';
            gatewayService.gateway = gateway;
            $modal.open(modal);
        };

        /*DELETE*/
        $scope.deleteGateway = function (gateway) {
            gatewayService.actionType = 'delete';
            gatewayService.gateway = gateway;
            $modal.open(confirmModal);
        };

        $scope.setSearchBy = function (field, label) {
            $scope.searchBy = field;
            $scope.searchByLabel = label;
        };

        gatewayService.handleCreate = function (gateway, $uibModalInstance) {
            gatewayService.getGatewayProvider().save(gateway).$promise.then(function (result) {
                if (result && result.code == 200) {
                    $scope.rowCollection.unshift(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_GATEWAY_CREATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_GATEWAY_CREATE'));
            }, function (ignored) {
                toastr.error(translate('MSG_ERROR_GATEWAY_CREATE'));
            });
        };

        gatewayService.handleUpdate = function (gateway, $uibModalInstance) {
            gatewayService.getGatewayProvider().save(gateway).$promise.then(function (result) {
                if (result && result.code == 200) {
                    updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_GATEWAY_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_GATEWAY_UPDATE'));
            }, function (ignored) {
                toastr.error(translate('MSG_ERROR_GATEWAY_UPDATE'));
            });
        };

        function updateTableRow(updatedItem) {
            for (var i = 0; i < $scope.rowCollection.length; i++) {
                if ($scope.rowCollection[i].id == updatedItem.id) {
                    angular.extend($scope.rowCollection[i], updatedItem);
                    break;
                }
            }
        }

        function removeTableRow(item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.rowCollection.length; i++)
                    if ($scope.rowCollection[i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.rowCollection.splice(index, 1);
            }
        }

        gatewayService.handleDelete = function (gateway, $uibModalInstance) {
            gatewayService.getGatewayProvider().delete({id: gateway.id}).$promise.then(function (result) {
                if (result && result.code == 200) {
                    removeTableRow(gateway);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_GATEWAY_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_GATEWAY_DELETE'));
            }, function (ignored) {
                toastr.error(translate('MSG_ERROR_GATEWAY_DELETE'));
            });
        };

        gatewayService.addTableRow = function (item) {
            $scope.numbers[item.gateway_id].unshift(item);
        };

        gatewayService.updateTableRow = function (updatedItem) {
            $scope.numbers[updatedItem.gateway_id].filter(function (number) {
                return number.id == updatedItem.id
            }).forEach(function (number) {
                angular.extend(number, updatedItem)
            });
        };

        gatewayService.removeTableRow = function (item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.numbers[item.gateway_id].length; i++)
                    if ($scope.numbers[item.gateway_id][i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.numbers[item.gateway_id].splice(index, 1);
            }
        };

        gatewayService.getAvailableChannels = function (number, actionType) {
            var availableChannels = [];
            for (var i = 1; i <= number.Gateway.number_of_channels; i++) {
                var exist = false;
                for (var j = 0; j < $scope.numbers[number.Gateway.id].length; j++) {
                    if (i == $scope.numbers[number.Gateway.id][j].channel_number && $scope.numbers[number.Gateway.id].enabled) {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                    availableChannels.push(i);
            }
            if (actionType == 'update')
                availableChannels.push(number.channel_number);
            return availableChannels;
        };
    }

    function GatewaysActionsController($scope, $uibModalInstance, gatewayService, countryService) {
        $scope.gateway = gatewayService.getGateway();
        $scope.actionTypeLabel = gatewayService.actionType == 'create' ? 'ADD' : 'EDIT';

        if (gatewayService.actionType == 'create' || gatewayService.actionType == 'update')
            $scope.countries = countryService.getCountryProvider().query();

        $scope.ok = function () {
            if (gatewayService.actionType == 'delete') {
                gatewayService.handleDelete($scope.gateway, $uibModalInstance);
            }
            else {
                if ($scope.gatewayForm.$valid) {
                    if (gatewayService.actionType == 'create') {
                        gatewayService.handleCreate($scope.gateway, $uibModalInstance);
                    }
                    else if (gatewayService.actionType == 'update')
                        gatewayService.handleUpdate($scope.gateway, $uibModalInstance);
                }
                else {
                    $scope.gatewayForm.submitted = true;
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    /**
     *SCHEDULER CONTROLLER
     */
    function SchedulerController($scope, $rootScope, $filter, toastr, $modal, schedulerService, AclService) {
        var translate = $filter('translate');
        $scope.can = AclService.can;

        var getBinaryArray = function (number, size) {
            if (!number) return "";
            var binary = number.toString(2);
            while (binary.length < size) binary = "0" + binary;
            return binary.split("").map(Number);
        };

        var getParameters = function () {
            var params = {};
            var exist = false;
            $rootScope.currentUser.Roles.forEach(function (role) {
                if (role.id == 1) {
                    exist = true;
                }
            });
            if (!exist)
                params['user_id'] = $rootScope.currentUser.id;
            return params;
        };

        var loadTasks = function () {
            var params = getParameters();
            schedulerService.getSchedulerProvider(params).query(params).$promise.then(function (tasks) {
                for (var i = 0; i < tasks.length; i++)
                    tasks[i].week_days = getBinaryArray(tasks[i].days, 7);
                $scope.rowCollection = tasks;
            });
        };
        loadTasks();

        schedulerService.handleCreate = function (task, $uibModalInstance) {
            schedulerService.getSchedulerProvider().save(task).$promise.then(function (result) {
                if (result && result.code == 200) {
                    result.data.week_days = getBinaryArray(result.data.days, 7);
                    $scope.rowCollection.unshift(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_TASK_CREATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_TASK_CREATE'));
            }, function (ignored) {
                toastr.error(translate('MSG_ERROR_TASK_CREATE'));
            });
        };

        var getTime = function (date) {
            if (!date) return null;
            return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        };

        schedulerService.handleUpdate = function (task, $uibModalInstance, status) {
            var translateLabel = 'MSG_TASK_UPDATED';
            status.flag = true;
            schedulerService.getSchedulerProvider().save(task).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    updateTableRow(result.data);
                    $uibModalInstance.close();
                    if (schedulerService.actionType == 'stop')
                        translateLabel = 'MSG_TASK_STOPPED';
                    else if (schedulerService.actionType == 'start')
                        translateLabel = 'MSG_TASK_STARTED';
                    toastr.success(translate(translateLabel));
                }
                else
                    toastr.error(translate('MSG_ERROR_TASK_UPDATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_TASK_UPDATE'));
            });
        };

        schedulerService.handleDelete = function (task, $uibModalInstance, status) {
            status.flag = true;
            schedulerService.getSchedulerProvider().delete({id: task.id}).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    removeTableRow(task);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_TASK_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_TASK_DELETE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_TASK_DELETE'));
            });
        };

        function updateTableRow(updatedItem) {
            for (var i = 0; i < $scope.rowCollection.length; i++) {
                if ($scope.rowCollection[i].id == updatedItem.id) {
                    angular.extend($scope.rowCollection[i], updatedItem);
                    break;
                }
            }
        }

        function removeTableRow(item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.rowCollection.length; i++)
                    if ($scope.rowCollection[i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.rowCollection.splice(index, 1);
            }
        }

        var modal = {
            templateUrl: 'views/scheduler/modals/scheduler-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: SchedulerActionsController,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.bootstrap.datetimepicker',
                            files: ['external/css/checkbox.css', 'external/js/plugins/datetime-picker.js']
                        }
                    ]);
                }
            }
        };

        var confirmModal = {
            templateUrl: 'views/scheduler/modals/scheduler-confirm-modal.html',
            backdrop: 'static',
            controller: SchedulerActionsController
        };

        /**CREATE*/
        $scope.createTask = function () {
            schedulerService.actionType = 'create';
            schedulerService.task = {};
            schedulerService.task.user_id = $rootScope.currentUser.id;
            schedulerService.task.active = 1;
            schedulerService.task.week_days = [0, 0, 0, 0, 0, 0, 0];
            $modal.open(modal);
        };

        var getDateTime = function (time) {
            if (!time) return null;
            var date = new Date();
            var parts = time.split(":");
            return new Date(date.setHours(parts[0], parts[1], parts[2]));
        };

        /**UPDATE*/
        $scope.updateTask = function (task) {
            task.start_datetime = getDateTime(task.start_time);
            task.end_datetime = getDateTime(task.end_time);
            schedulerService.actionType = 'update';
            schedulerService.task = task;
            schedulerService.task.week_days = getBinaryArray(task.days);
            $modal.open(modal);
        };

        /**DELETE*/
        $scope.deleteTask = function (task) {
            schedulerService.actionType = 'delete';
            schedulerService.task = task;
            $modal.open(confirmModal);
        };

        /**STOP*/
        $scope.stopTask = function (task) {
            schedulerService.actionType = 'stop';
            schedulerService.task = task;
            $modal.open(confirmModal);
        };

        /**START*/
        $scope.startTask = function (task) {
            schedulerService.actionType = 'start';
            schedulerService.task = task;
            $modal.open(confirmModal);
        };

        $scope.isExpired = function (task) {
            return task.scheduler_end != null && new Date(task.scheduler_end) < new Date();
        };

        $scope.isRecurring = function (task) {
            return task.scheduler_end == null;
        };

        $scope.getProgressValue = function (task) {
            var date1 = new Date(task.scheduler_start);
            var date3 = new Date(new Date());
            var date2 = new Date(task.scheduler_end);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var pastDiff = Math.abs(date2.getTime() - date3.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var pastDiffDays = Math.ceil(pastDiff / (1000 * 3600 * 24));
            return Math.round(((diffDays - pastDiffDays) * 100) / diffDays);
        }
    }

    function SchedulerActionsController($scope, $uibModalInstance, $state, schedulerService, userService) {
        $scope.actionTypeLabel = schedulerService.actionType == 'create' ? 'ADD' : 'EDIT';
        $scope.actionType = schedulerService.actionType;
        $scope.status = {flag: false};
        $scope.task = schedulerService.getTask();
        $scope.isStartDateOpen = false;
        $scope.isEndDateOpen = false;
        $scope.isStartTimeOpen = false;
        $scope.isEndTimeOpen = false;
        $scope.showMeridian = true;

        if (schedulerService.actionType == 'create' || schedulerService.actionType == 'update')
            $scope.users = userService.getUserProvider().query();

        $scope.openPicker = function (e) {
            e.preventDefault();
            e.stopPropagation();
            $scope.isStartDateOpen = false;
            $scope.isEndDateOpen = false;
            $scope.isStartTimeOpen = false;
            $scope.isEndTimeOpen = false;
            if (e.target.id == 'startDateButton')
                $scope.isStartDateOpen = true;
            else if (e.target.id == 'endDateButton')
                $scope.isEndDateOpen = true;
            else if (e.target.id == 'startTimeButton')
                $scope.isStartTimeOpen = true;
            else if (e.target.id == 'endTimeButton')
                $scope.isEndTimeOpen = true;
        };

        $scope.ok = function () {
            if (schedulerService.actionType == 'delete') {
                schedulerService.handleDelete($scope.task, $uibModalInstance, $scope.status);
            }
            else if (schedulerService.actionType == 'close') {
                $uibModalInstance.close();
                $state.go('index.scheduler');
            }
            else if (schedulerService.actionType == 'stop') {
                $scope.task.active = 0;
                schedulerService.handleUpdate($scope.task, $uibModalInstance, $scope.status);
            }
            else if (schedulerService.actionType == 'start') {
                $scope.task.active = 1;
                schedulerService.handleUpdate($scope.task, $uibModalInstance, $scope.status);
            }
            else {
                $scope.task.start_time = getTime($scope.task.start_datetime);
                $scope.task.end_time = getTime($scope.task.end_datetime);
                if ($scope.schedulerForm.$valid) {
                    if (schedulerService.actionType == 'create') {
                        $scope.task.days = parseInt($scope.task.week_days.join(''), 2);
                        schedulerService.handleCreate($scope.task, $uibModalInstance);
                    }
                    else if (schedulerService.actionType == 'update')
                        schedulerService.handleUpdate($scope.task, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.schedulerForm.submitted = true;
                }
            }
        };

        var getTime = function (date) {
            if (!date) return null;
            return date.getHours() + ":" + date.getMinutes() + ":00";
        };

        $scope.next = function () {
            if ($scope.schedulerForm.$valid) {
                $state.go('index.scheduler-wizard.confirmation-page');
            }
            else {
                $scope.schedulerForm.submitted = true;
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.setRandomValue = function (field) {
            if (field == 'deltaInterval')
                $scope.task.delta_interval = Math.floor((Math.random() * 40) + 10);
            else if (field == 'interval')
                $scope.task.interval = Math.floor((Math.random() * 115) + 5);
        };

        $scope.setCurrentDate = function (field) {
            if (field == 'startDate')
                $scope.task.scheduler_start = new Date();
            else if (field == 'endDate')
                $scope.task.scheduler_end = new Date();
            else if (field == 'startTime')
                $scope.task.start_datetime = new Date();
            else if (field == 'endTime')
                $scope.task.end_datetime = new Date();
        };

        var confirmModal = {
            templateUrl: 'views/scheduler/modals/scheduler-confirm-modal.html',
            backdrop: 'static',
            controller: SchedulerActionsController
        };

        /**CLOSE WIZARD*/
        $scope.closeWizard = function () {
            schedulerService.actionType = 'close';
            schedulerService.task = {};
            $uibModalInstance.open(confirmModal);
        };
    }

    /**
     *ROLES AND PRIVILEGES CONTROLLER
     */
    function RolesPrivilegesController($scope, AclService) {
        $scope.can = AclService.can;
    }

    /**
     *MANAGEMENT CONTROLLER
     */
    function ManagementController($scope, AclService) {
        $scope.can = AclService.can;
    }

    /**
     *ORDERS CONTROLLER
     */
    function OrdersController($scope, AclService) {
        $scope.can = AclService.can;
    }

    /**
     *TICKETS CONTROLLER
     */
    function TicketsController($scope, $rootScope,$stateParams, $modal, toastr, $filter, ticketService, userService, AclService) {
        var translate = $filter('translate');
        $scope.itemsByPage = 15;
        $scope.can = AclService.can;
        $scope.columns = {tracker_id:'TRACKER',status_id:'STATUS',priority_id:'PRIORITY', subject:'SUBJECT'};

        if ($stateParams.id)
            ticketService.getTicketProvider({id: $stateParams.id}).then(function(ticket){
                $scope.ticket = ticket.data.issue;
                $scope.ticket.comments = $scope.ticket.journals.filter(function(journal){return journal.notes != ''});
                $scope.ticket.activities = $scope.ticket.journals.filter(function(journal){return journal.notes == ''});
                userService.getUserIssueProvider().query(getParameters()).$promise.then(function (result) {
                        result.forEach(function (item) {
                                if (item.issue_id == $stateParams.id) {
                                    $scope.ticket.User = item.User;
                                }
                        });
                    }
                );
            });

        var getParameters = function () {
            var params = {};
            var exist = false;
            $rootScope.currentUser.Roles.forEach(function (role) {
                if (role.id == 1) {
                    exist = true;
                }
            });
            if (!exist)
                params['user_id'] = $rootScope.currentUser.id;
            return params;
        };

        ticketService.getTicketProvider(getParameters()).then(function (data) {
                var tickets = data.data.issues;
                var filteredTickets = [];
                userService.getUserIssueProvider().query(getParameters()).$promise.then(function (result) {
                        result.forEach(function (item) {
                            tickets.forEach(function (ticket) {
                                if (item.issue_id == ticket.id) {
                                    ticket.User = item.User;
                                    filteredTickets.push(ticket);
                                }
                            })
                        });
                    }
                );
                $scope.rowCollection = filteredTickets;
            }
        );

        var modal = {
            templateUrl: 'views/tickets/modals/ticket-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: TicketsActionsController
        };

        var confirmModal = {
            templateUrl: 'views/tickets/modals/ticket-confirm-modal.html',
            backdrop: 'static',
            controller: TicketsActionsController
        };

        $scope.createTicket = function () {
            ticketService.actionType = 'create';
            ticketService.ticket = {
                status_id: 1, category_id: 2, author_id: 10, assigned_to_id: 10,
                project_id: 2, tracker_id: 7, lock_version: 0, done_ratio: 0, is_private: 0
            };
            $modal.open(modal);
        };

        $scope.updateTicket = function (ticket) {
            ticketService.actionType = 'update';
            ticketService.ticket = ticket;
            $modal.open(modal);
        };

        $scope.closeTicket = function (ticket) {
            ticketService.actionType = 'close';
            ticketService.ticket = ticket;
            $modal.open(confirmModal);
        };

        function updateTableRow(updatedItem) {
            for (var i = 0; i < $scope.rowCollection.length; i++) {
                if ($scope.rowCollection[i].id == updatedItem.id) {
                    angular.extend($scope.rowCollection[i], updatedItem);
                    break;
                }
            }
        }

        function removeTableRow(item) {
            if (item) {
                var index = -1;
                for (var i = 0; i < $scope.rowCollection.length; i++)
                    if ($scope.rowCollection[i].id == item.id)
                        index = i;
                if (index != -1)
                    $scope.rowCollection.splice(index, 1);
            }
        }

        ticketService.handleCreate = function (ticket, $uibModalInstance, status) {
            status.flag = true;
            var data = {issue:ticket};
            ticketService.getTicketPostProvider(data).then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    $scope.rowCollection.unshift(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_TICKET_CREATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_TICKET_CREATE'));

            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_TICKET_CREATE'));
            });
        };

        ticketService.handleUpdate = function (ticket, $uibModalInstance, status) {
            status.flag = true;
            ticketService.getTicketProvider().save(ticket).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    updateTableRow(result.data);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_TICKET_UPDATED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_TICKET_UPDATE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_TICKET_UPDATE'));
            });
        };

        ticketService.handleDelete = function (ticket, $uibModalInstance, status) {
            status.flag = true;
            ticketService.getTicketProvider().delete({id: ticket.id}).$promise.then(function (result) {
                status.flag = false;
                if (result && result.code == 200) {
                    removeTableRow(ticket);
                    $uibModalInstance.close();
                    toastr.success(translate('MSG_TICKET_DELETED'));
                }
                else
                    toastr.error(translate('MSG_ERROR_TICKET_DELETE'));
            }, function (ignored) {
                status.flag = false;
                toastr.error(translate('MSG_ERROR_TICKET_DELETE'));
            });
        };
    }

    function TicketsActionsController($scope, $uibModalInstance, ticketService) {
        if (ticketService.actionType == 'create' || ticketService.actionType == 'update') {
            ticketService.getTicketStatusProvider().then(function (data) {
                $scope.statuses = data.data.issue_statuses;
            });
            ticketService.getTicketPriorityProvider().then(function (data) {
                $scope.priorities = data.data.issue_priorities;
            });
            $scope.categories = ticketService.getTicketCategoryProvider().then(function (data) {
                $scope.categories = data.data.issue_categories;
            });
        }
        $scope.status = {flag: false};
        $scope.ticket = ticketService.getTicket();
        $scope.actionTypeLabel = ticketService.actionType == 'create' ? 'ADD' : 'EDIT';

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            if (ticketService.actionType == 'close') {
                $scope.ticket.status_id = 5;
                ticketService.handleUpdate($scope.ticket, $uibModalInstance, $scope.status);
            }
            else {
                if ($scope.ticketForm.$valid) {
                    if (ticketService.actionType == 'create') {
                        $scope.ticket.created_on = new Date();
                        ticketService.handleCreate($scope.ticket, $uibModalInstance, $scope.status);
                    }
                    else if (ticketService.actionType == 'update')
                        ticketService.handleUpdate($scope.ticket, $uibModalInstance, $scope.status);
                }
                else {
                    $scope.ticketForm.submitted = true;
                }
            }
        };
    }

    /**
     *CONFIGURATION CONTROLLER
     */
    function ConfigurationController($scope, AclService) {
        $scope.can = AclService.can;
    }

    /**
     * ADD CONTROLLERS TO MODULE
     */
    angular.module('clipper')
        .controller('AuthenticationController', ['$scope', '$auth', 'toastr', '$filter', '$translate', '$state', '$rootScope', '$stateParams', 'AclService', 'userService', AuthenticationController])
        .controller('TranslateController', ['$filter', '$translate', '$state', '$scope', '$rootScope', 'toastr', 'userService', TranslateController])
        .controller('DashboardController', ['$scope', '$http', '$rootScope', '$filter', '$uibModal', 'cdrService', 'operatorService', 'ticketService', 'reportService', 'AclService', DashboardController])
        .controller('UsersController', ['$scope', '$uibModal', 'userService', 'toastr', '$filter', 'AclService', UsersController])
        .controller('CdrController', ['$scope', '$rootScope', 'cdrService', '$uibModal', 'AclService', CdrController])
        .controller('CountriesController', ['$scope', '$uibModal', 'countryService', 'toastr', '$translate', 'AclService', CountriesController])
        .controller('OperatorsController', ['$scope', '$uibModal', 'operatorService', 'toastr', '$filter', 'AclService', OperatorsController])
        .controller('OperatorCodesController', ['$scope', '$uibModal', 'operatorService', 'toastr', '$filter', OperatorCodesController])
        .controller('GatewaysController', ['$scope', '$uibModal', 'gatewayService', 'toastr', '$filter', 'AclService', GatewaysController])
        .controller('SchedulerController', ['$scope', '$rootScope', '$filter', 'toastr', '$uibModal', 'schedulerService', 'AclService', SchedulerController])
        .controller('RolesPrivilegesController', ['$scope', 'AclService', RolesPrivilegesController])
        .controller('ManagementController', ['$scope', 'AclService', ManagementController])
        .controller('OrdersController', ['$scope', 'AclService', OrdersController])
        .controller('TicketsController', ['$scope', '$rootScope', '$stateParams', '$uibModal', 'toastr', '$filter', 'ticketService', 'userService', 'AclService', TicketsController])
        .controller('ConfigurationController', ['$scope', 'AclService', ConfigurationController])
        .controller('NumbersController', ['$scope', '$uibModal', 'gatewayService', 't,oastr', '$filter', 'AclService', NumbersController])
        .controller('SchedulerActionsController', ['$scope', '$uibModal', '$state', 'schedulerService', 'userService', SchedulerActionsController])
})
();
