(function () {

    /**
     * pageTitle - Directive for set Page title - mata title
     */
    function pageTitle($rootScope, $timeout) {
        return {
            link: function (scope, element) {
                var listener = function (event, toState, toParams, fromState, fromParams) {
                    // Default title - load on Dashboard 1
                    var title = 'Clipper';
                    // Create your own title pattern
                    if (toState.data && toState.data.pageTitle) title = 'Clipper | ' + toState.data.pageTitle;
                    $timeout(function () {
                        element.text(title);
                    });
                };
                $rootScope.$on('$stateChangeStart', listener);
            }
        }
    }

    /**
     * sideNavigation - Directive for run metsiMenu on sidebar navigation
     */
    function sideNavigation($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                // Call the metsiMenu plugin and plug it to sidebar navigation
                $timeout(function () {
                    element.metisMenu();
                });
            }
        };
    }

    /**
     * minimalizaSidebar - Directive for minimalize sidebar
     */
    function minimalizaSidebar($timeout) {
        return {
            restrict: 'A',
            template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
            controller: function ($scope, $element) {
                $scope.minimalize = function () {
                    $("body").toggleClass("mini-navbar");
                    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                        // Hide menu in order to smoothly turn on when maximize menu
                        $('#side-menu').hide();
                        // For smoothly turn on menu
                        setTimeout(
                            function () {
                                $('#side-menu').fadeIn(400);
                            }, 200);
                    } else if ($('body').hasClass('fixed-sidebar')) {
                        $('#side-menu').hide();
                        setTimeout(
                            function () {
                                $('#side-menu').fadeIn(400);
                            }, 100);
                    } else {
                        // Remove all inline style from jquery fadeIn function to reset menu state
                        $('#side-menu').removeAttr('style');
                    }
                }
            }
        };
    }

    function checkPassword() {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.checkPassword;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        }
    }

    function showValidation() {
        return {
            restrict: 'A',
            require: '^form',
            link: function (scope, element, attrs, form) {
                var name, type, inputElement, toggleClasses;
                inputElement = angular.element(element[0].querySelector('[name]'));
                name = inputElement.attr('name');
                type = inputElement.attr('type');
                if (form)
                    form.submitted = false;
                scope.$watch(function () {
                    if (form && form.submitted)
                        return toggleClasses(form[name].$invalid);
                });
                scope.$on(function () {
                    if (form && form.submitted)
                        return toggleClasses(form[name].$invalid);
                });
                return toggleClasses = function (invalid) {
                    element.toggleClass('has-error', invalid);
                    return element.toggleClass('has-success', !invalid);
                };
            }
        };
    }

    function toggleControl($timeout) {
        return {
            restrict: 'A',
            scope: true,
            templateUrl: 'views/common/toggle-control.html',
            controller: function ($scope, $element) {
                // Function for collapse ibox
                $scope.showhide = function () {
                    var ibox = $element.closest('div.ibox');
                    var icon = $element.find('i:first');
                    var content = ibox.find('div.ibox-content');
                    content.slideToggle(200);
                    // Toggle icon from up to down
                    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    $timeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                };
            }
        };
    }

    /**
     * sparkline - Directive for Sparkline chart
     */
    function sparkline() {
        return {
            restrict: 'A',
            scope: {
                sparkData: '=',
                sparkOptions: '=',
            },
            link: function (scope, element, attrs) {
                scope.$watch(scope.sparkData, function () {
                    render();
                });
                scope.$watch(scope.sparkOptions, function () {
                    render();
                });
                var render = function () {
                    $(element).sparkline(scope.sparkData, scope.sparkOptions);
                };
            }
        }
    };


    /**
     * clockPicker - Directive for clock picker plugin
     */
    function clockPicker() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.clockpicker();
            }
        };
    }

    function watchChange() {
        return {
            scope: {
                onchange: '&watchChange'
            },
            link: function(scope, element, attrs) {
                element.on('input', function() {
                    scope.$apply(function () {
                        scope.onchange();
                    });
                });
            }
        };
    }

    /**
     *
     * Pass all functions into module
     */
    angular.module('clipper')
        .directive('pageTitle', pageTitle)
        .directive('sideNavigation', sideNavigation)
        .directive('minimalizaSidebar', minimalizaSidebar)
        .directive('checkPassword', checkPassword)
        .directive('toggleControl', toggleControl)
        .directive('showValidation', showValidation)
        .directive('sparkline', sparkline)
        .directive('watchChange', watchChange)
        .directive('clockPicker', clockPicker);
})();