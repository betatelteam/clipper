/**
 * Created by Muris Agic on 08/08/2016.
 */
function getApiUrl(path) {
    return 'http://localhost:9000/api'.concat(path);
}

function getRedmineUrl(path) {
    return 'http://212.18.46.52/redmine'.concat(path);
}

function getApiKey() {
    return 'd96359ecb04f5b8ee6e6bc0b9e5220ad63ca2eae';
}

angular.module('clipper')
    .service("userService", ['$resource', function ($resource) {
        this.actionType = {};
        this.user = {};

        this.getUserProvider = function () {
            return $resource(getApiUrl('/users/:userId'))
        };

        this.getUserProvider = function () {
            return $resource(getApiUrl('/users/:userId'))
        };

        this.getUserIssueProvider = function () {
            return $resource(getApiUrl('/users/:userId/issues'))
        };

        this.getUserLanguageProvider = function () {
            return $resource(getApiUrl('/change_language'))
        };

        this.getLanguageProvider = function () {
            return $resource(getApiUrl('/languages/:languageId'))
        };

        this.getRoleProvider = function () {
            return $resource(getApiUrl('/roles/:roleId'))
        };

        this.getRolePrivilegeProvider = function () {
            return $resource(getApiUrl('/role_privileges/:rolePrivilegeId'))
        };

        this.getStatusProvider = function () {
            return $resource(getApiUrl('/user_statuses/:statusId'))
        };

        this.getUser = function () {
            return angular.copy(this.user)
        };
    }])
    .service("cdrService", ['$resource', function ($resource) {
        this.actionType = {};
        this.calls = {};

        this.getOutgoingCallProvider = function (type, params) {
            var url = '/outgoing_calls';
            var parameters = {
                all: '', date: '/date/:date', week: '/year/:year/week/:week',
                month: '/year/:year/month/:month', year: '/year/:year', user: '/user/:user_id',
                operator: '/operator/:operator_id'
            };
            /**USER*/
            if (params['user_id'])
                url += parameters['user'];

            /**OPERATOR*/
            if (params['operator_id'])
                url += parameters['operator'];

            return $resource(getApiUrl(url + parameters[type]));
        };

        this.getIncomingCallProvider = function () {
            return $resource(getApiUrl('/outgoing_calls/:call_id/incoming_calls'))
        };

        this.getStatusProvider = function () {
            return $resource(getApiUrl('/call_statuses'))
        };

        this.makeCall = function () {
            return $resource(getApiUrl('/new_call'))
        };

        this.getCalls = function () {
            return this.calls;
        };
    }])
    .service("operatorService", ['$resource', function ($resource) {
        this.actionType = {};
        this.operator = {};
        this.operatorCode = {};

        this.getOperatorProvider = function () {
            return $resource(getApiUrl('/operators/:operatorId'))
        };

        this.getOperatorCodeProvider = function (type) {
            if (type == "all")
                $resource(getApiUrl('/operator_codes'));
            return $resource(getApiUrl('/operators/:operatorId/operator_codes'))
        };

        this.getOperatorInboundProvider = function (params) {
            var url = '/operators/inbound';
            /**USER*/
            if (params['user_id'])
                url += '/user/:user_id';
            return $resource(getApiUrl(url));
        };

        this.getOperator = function () {
            return angular.copy(this.operator)
        };

        this.getOperatorCode = function () {
            return angular.copy(this.operatorCode)
        };
    }])
    .service("countryService", ['$resource', function ($resource) {
        this.actionType = {};
        this.country = {};

        this.getCountryInboundProvider = function (params) {
            var url = '/countries/inbound';
            /**USER*/
            if (params['user_id'])
                url += '/user/:user_id';
            return $resource(getApiUrl(url))
        };

        this.getCountryProvider = function () {
            return $resource(getApiUrl('/countries/:countryId'))
        };

        this.getCountry = function () {
            return angular.copy(this.country)
        };
    }])
    .service("gatewayService", ['$resource', function ($resource) {
        this.actionType = {};
        this.gateway = {};
        this.number = {};

        this.getGatewayProvider = function () {
            return $resource(getApiUrl('/gateways/:gatewayId'))
        };

        this.getNumberProvider = function () {
            return $resource(getApiUrl('/numbers'))
        };

        this.getGatewayNumberProvider = function () {
            return $resource(getApiUrl('/gateways/:gatewayId/numbers'))
        };

        this.getGateway = function () {
            return angular.copy(this.gateway)
        };

        this.getNumber = function () {
            return angular.copy(this.number)
        };
    }])
    .service("reportService", ['$resource', function ($resource) {
        this.getCallReportProvider = function (type, params) {
            var url = '/report/calls';
            var parameters = {
                all: '', date: '/date/:date', week: '/year/:year/week/:week',
                month: '/year/:year/month/:month', year: '/year/:year', user: '/user/:user_id',
                operator: '/operator/:operator_id'
            };
            /**USER*/
            if (params['user_id'])
                url += parameters['user'];

            /**OPERATOR*/
            if (params['operator_id'])
                url += parameters['operator'];

            return $resource(getApiUrl(url + parameters[type]));
        };

        this.getOperatorReportProvider = function (type, params) {
            var url = '/report/operators';
            var parameters = {
                all: '', date: '/date/:date', week: '/year/:year/week/:week',
                month: '/year/:year/month/:month', year: '/year/:year', user: '/user/:user_id',
                operator: '/operator/:operator_id'
            };
            /**USER*/
            if (params['user_id'])
                url += parameters['user'];
            /**OPERATOR*/
            if (params['operator_id'])
                url += parameters['operator'];

            return $resource(getApiUrl(url + parameters[type]));
        };

        this.getOperatorMonthlyReportProvider = function (params) {
            var url = '/report/operators_monthly';
            /**USER*/
            if (params['user_id'])
                url += '/user/:user_id';
            /**OPERATOR*/
            if (params['operator_id'])
                url += '/operator/:operator_id';
            return $resource(getApiUrl(url));
        };
    }])
    .service("schedulerService", ['$resource', function ($resource) {
        this.actionType = {};
        this.task = {};

        this.getSchedulerProvider = function (params) {
            if (params && params['user_id'])
                return $resource(getApiUrl('/scheduled_tasks/user/:user_id'));
            else
                return $resource(getApiUrl('/scheduled_tasks'));
        };

        this.getSchedulerRefreshProvider = function () {
            return $resource(getApiUrl('/refresh_scheduler'));
        };

        this.getTask = function () {
            return angular.copy(this.task)
        };
    }])
    .service("ticketService", ['$http', function ($http) {
        this.actionType = {};
        this.ticket = {};
        var url_extend = '.json?key=' + getApiKey() + '&callback=JSON_CALLBACK';

        this.getTicketStatusProvider = function () {
            var params = '/issue_statuses' + url_extend;
            return this.get(getRedmineUrl(params));
        };

        this.getTicketPriorityProvider = function () {
            var params = '/enumerations/issue_priorities' + url_extend;
            return this.get(getRedmineUrl(params));
        };

        this.getTicketCategoryProvider = function () {
            var params = '/projects/2/issue_categories' + url_extend;
            return this.get(getRedmineUrl(params));
        };

        this.getTicketProvider = function (parameters) {
            var params = '/issues';
            var auth = '?key=' + getApiKey() + '&callback=JSON_CALLBACK&include=journals,watchers,attachments,changesets&tracker_id=7';
            if (parameters && parameters['id'])
                params += '/' + parameters['id'];
            params += '.json' + auth;
            return this.get(getRedmineUrl(params));
        };

        this.getTicketPostProvider = function (data) {
            var params = '/issues.json?key=' + getApiKey()+'&callback=JSON_CALLBACK';
            var body = JSON.stringify(data);
            return this.post(getRedmineUrl(params), body);
        };

        this.get = function (url) {
            return $http.jsonp(url).success(function (data) {
                return {code: 200, data: data};
            }).error(function (error) {
                return {code: 100, error: error};
            });
        };

        this.post = function (url, data) {
            var data = {
                issue: {
                    project_id: 1,
                    subject: 'Example',
                    priority_id: 4
                }
            };
            $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
            $http.defaults.useXDomain=true;
            // Send this header only in post requests. Specifies you are sending a JSON object
            $http.defaults.headers.post['dataType'] = 'json'
            return $http({
                url: url,
                method: "POST",
                headers: {
                    'Content-Type': "application/json; charset=UTF-8"
                },
                data: JSON.stringify(data)
            }).success(function (data) {
                return {code: 200, data: data};
            }).error(function (error) {
                return {code: 100, error: error};
            });
        };

        this.getTicket = function () {
            return angular.copy(this.ticket)
        };
    }]);